package guava;

import com.google.common.base.Splitter;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class SplitterTest {
    @Test
    public void test01() {
        List<String> strings2 = Splitter.on("|").splitToList("hello|world");
        System.out.println(strings2);
        // 忽略空字符串
        List<String> strings3 = Splitter.on("|").omitEmptyStrings().splitToList("hello|world|||");
        System.out.println(strings3);
        // 每n个字符串截取一下
        List<String> strings = Splitter.fixedLength(3).splitToList("1234567890");
        System.out.println(strings);
    }

    /**
     * 分割后直接变成map
     */
    @Test
    public void splitMap() {
        Map<String, String> split = Splitter.on(Pattern.compile("\\|")).omitEmptyStrings().trimResults().withKeyValueSeparator('=').split("a=hello | b=world");
        System.out.println(split);
    }
}