package guava;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class JoinerTest {
    private final List<String> list1 = Arrays.asList("a", "b", "c", "d");
    private final List<String> list2 = Arrays.asList("a", "b", "c", "d", null);
    private final Map<String, String> stringMap = ImmutableMap.of("a", "b", "c", "d");

    @Test
    public void test01() {
        String join = Joiner.on("#").join(list1);
        System.out.println(join);
        String join2 = Joiner.on("#").skipNulls().join(list2);
        System.out.println(join2);
    }

    @Test
    public void testMap() {
        String join = Joiner.on('#').withKeyValueSeparator(',').join(stringMap);
        System.out.println(join);
    }
}
