package com.swqsv.cofeeol.问题演练;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Test01 {
    List<Map<String, Object>> dataList = Lists.newArrayList();

    @BeforeEach
    public void before() {
        // "invoiceCode" 发票代码
        // "invoiceNo" 发票序号
        // "lineId" 报账行号
        // "tax" 税额
        for (int i = 0; i < 5; i++) {
            Map<String, Object> data = Maps.newHashMap();
            data.put("invoiceCode", String.valueOf(3100 + i));
            data.put("invoiceNo", String.valueOf(825 + i));
            data.put("lineId", String.valueOf(6025 + i));
            // data.put("tax", String.valueOf(100.5 + i));
            dataList.add(data);
        }
        Map<String, Object> data = Maps.newHashMap();
        data.put("invoiceCode", String.valueOf(3100));
        data.put("invoiceNo", String.valueOf(825));
        data.put("lineId", String.valueOf(6025));
        // data.put("tax", String.valueOf(200.5));
        dataList.add(data);
    }


    @Test
    public void Test() {
        Stopwatch watch = Stopwatch.createStarted();
        System.out.println("dataList: " + dataList);
        // 给dataList分组
        Map<Object, List<Map<String, Object>>> invoiceCodeMap = dataList.stream().collect(Collectors.groupingBy(map -> map.get("invoiceCode")));
        // System.out.println("invoiceCodeMap: " + invoiceCodeMap);
        List<HashMap<String, Object>> collect = dataList.stream().map(map -> {
            HashMap<String, Object> newHashMap = Maps.newHashMap();
            String invoiceCode = map.get("invoiceCode").toString();
            String invoiceNo = map.get("invoiceNo").toString();
            newHashMap.put("special_id", invoiceCode + invoiceNo);
            // 这两个是相同的,可以怎么优化呢?
            // newHashMap.put("lineId", map.get("lineId").toString());
            // newHashMap.put("tax", map.get("tax").toString());
            /*Map<String, Integer> map = new HashMap<>();
            map.put("name", 1);
            // name 在原来的map是有的, (oldValue, newValue) -> oldValue + newValue 这个给name
            map.merge("name", 1, (oldValue, newValue) -> oldValue + newValue);
            // count 在原来的map是没有的所以就put一下,把1给count
            map.merge("count", 1, (oldValue, newValue) -> oldValue + newValue);
            System.out.println(map);*/

            // 1. newHashMap中不需要invoiceCode,invoiceNo 所以我们先过滤掉,然后添加
            map.keySet().stream()
                    .filter(k -> !("invoiceCode".equals(k) || "invoiceNo".equals(k)))// 过滤掉这两个不需要的,其他需要的重新插进来
                    .collect(Collectors.toList())
                    .forEach(k -> {
                        String value = map.get(k).toString();
                        newHashMap.merge(k, value, (e1, e2) -> e2);
                    });
            return newHashMap;
        }).collect(Collectors.toList());
        System.out.println("collect:" + collect);
        System.out.println(watch.elapsed(TimeUnit.MILLISECONDS));
    }
}
