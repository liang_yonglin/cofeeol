package 解决问题;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Employee {

    private Long id;
    private String name;


    // 此处省略构造方法, getters, setters方法
}
