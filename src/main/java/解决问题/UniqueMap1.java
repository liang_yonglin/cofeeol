package 解决问题;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author shengshu
 */
public class UniqueMap1 {

    // Transfer to sorted Map
    public static Map<String, String> transferToSortedMap(Map<String, String> map) {
        // Define comparator for TreeMap
        // Note: Sort according to descending, because retain the smaller Key's record when exchanging Map's Key and Value
        Map<String, String> sort_map = new TreeMap<String, String>(new Comparator<String>() {
            @Override
            public int compare(String key1, String key2) {
                return key2.hashCode() - key1.hashCode();
            }
        });

        sort_map.putAll(map);

        return sort_map;
    }

    // Exchange Map's Key and Value
    public static Map<String, String> exchangeMap(Map<String, String> map) {
        Map<String, String> exchange_map = new TreeMap<String, String>();

        for (String key : map.keySet()) {
            String value = map.get(key);

            exchange_map.put(value, key);
        }

        return exchange_map;
    }

    // Print Map
    public static void printMap(Map<String, String> map) {
        Iterator<Entry<String, String>> iterator = map.entrySet().iterator();

        while (iterator.hasNext()) {
            Entry<String, String> entry = iterator.next();

            String key = entry.getKey();
            String value = entry.getValue();

            System.out.println(key + " --> " + value);
        }
    }

    public static void main(String[] args) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("A", "1");
        map.put("C", "3");
        map.put("D", "2");
        map.put("B", "3");
        map.put("E", "3");

        // Sort Map by descending order
        // Note: Sort according to descending, because retain the smaller Key's record when exchanging Map's Key and Value
        Map<String, String> sort_map = UniqueMap1.transferToSortedMap(map);

        // Exchange Key and Value for overlapping repetition record
        Map<String, String> exchange_map = UniqueMap1.exchangeMap(sort_map);

        // Exchange Map for recovering Key and Value, this Map is what we want
        exchange_map = UniqueMap1.exchangeMap(exchange_map);

        // Print Map
        UniqueMap1.printMap(exchange_map);
    }
}