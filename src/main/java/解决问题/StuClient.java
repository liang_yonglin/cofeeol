package 解决问题;

import java.util.HashMap;
import java.util.Map;

public class StuClient {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        map.put("name", 1);
        // name 在原来的map是有的, (oldValue, newValue) -> oldValue + newValue 这个给name
        map.merge("name", 1, (oldValue, newValue) -> oldValue + newValue);
        // count 在原来的map是没有的所以就put一下,把1给count
        map.merge("count", 1, (oldValue, newValue) -> oldValue + newValue);
        System.out.println(map);
    }
}
