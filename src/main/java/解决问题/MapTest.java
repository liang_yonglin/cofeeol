package 解决问题;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

/**
 * 删除Map中值重复的键值对
 *
 * @author tang
 */
public class MapTest {

    public static void main(String[] args) {

        HashMap<Object, Object> map = new HashMap<>();
        map.put("k", "v");
        map.put("k2", "v");
        map.put("k3", "v");
        map.put("k4", "v2");
        map.put("k5", "v3");
        System.out.println(map);

        Set<Object> keyset1 = new HashSet<>();
        Set<Entry<Object, Object>> entrySet1 = map.entrySet();
        Set<Entry<Object, Object>> entrySet2 = map.entrySet();
        for (Entry<Object, Object> entry1 : entrySet1) {
            for (Entry<Object, Object> entry2 : entrySet2) {
                if (entry1 != entry2 && entry1.getValue().equals(entry2.getValue())) {
                    keyset1.add(entry1.getKey());
                    keyset1.add(entry2.getKey());
                }
            }
        }
        for (Object object : keyset1) {
            map.remove(object);
        }
        System.out.println(map);
    }
}
