package 解释器模式;

import java.util.HashMap;

/**
 * 加法解析器
 *
 * @author duguq
 */
public class AddExpression extends SymbolExpression {
    public AddExpression(Expression _left, Expression _right) {
        super(_left, _right);
    }

    /**
     * 把左右两个表达式运算的结果加起来
     *
     * @param var
     * @return
     */
    @Override
    public int interpreter(HashMap<String, Integer> var) {
        return super.left.interpreter(var) + super.right.interpreter(var);//把里面的变量获取,然后相加
    }
}