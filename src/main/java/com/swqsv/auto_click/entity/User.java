package com.swqsv.auto_click.entity;

import lombok.Data;

/**
 * @author duguq
 */
@Data
public class User {
    /**
     * 用户名
     */
    private String userName;
    /**
     * 会员等级
     */
    private String userLevel;
}