package com.swqsv.auto_click.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author duguq
 */
@Data
public class CommentPicture {
    /**
     * 图片地址
     */
    private String imageUrl;
    /**
     * 店铺链接
     */
    private String storeUrl;

    /**
     * 图片评论者
     */
    private User commentAuthor;

    /**
     * 评论时间
     */
    private Date commentTime;

    /**
     * 商品信息
     */
    private String orderInfo;
}


