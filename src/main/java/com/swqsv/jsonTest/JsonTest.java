package com.swqsv.jsonTest;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import java.lang.reflect.Type;

public class JsonTest {
    public static void main(String[] args) {
        ResponseA responseA = new ResponseA();
        ThirdBeanForA thirdBeanForA = new ThirdBeanForA();
        thirdBeanForA.setResultCode(12354678121256938L);
        thirdBeanForA.setResultMsg("你好啊两咋I");
        thirdBeanForA.setResultObject("哈哈哈");
        responseA.setThirdBean(thirdBeanForA);
        Gson gson = new Gson();
        String s = gson.toJson(responseA);
        System.out.println("s = " + s);

        Gson gson1 = new Gson();
        Type type = new TypeToken<Response<ThirdBeanForA>>() {
        }.getType();
        Response<ThirdBeanForA> response = gson1.fromJson(s, type);
        System.out.println("response = " + response);


//        Gson gson2 = new Gson();
//        ResponseA response2 = gson2.fromJson(s, ResponseA.class);
//        System.out.println("response2 = " + response2);
    }
}
