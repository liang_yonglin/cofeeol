package com.swqsv.jsonTest;

import lombok.Data;

@Data
public class ThirdBeanForA {
    private long resultCode;
    private String resultObject;
    private String resultMsg;
}
