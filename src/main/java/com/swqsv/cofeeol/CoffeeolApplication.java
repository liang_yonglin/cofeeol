package com.swqsv.cofeeol;

import com.swqsv.cofeeol.mapper.CoffeeMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author duguq
 */
@SpringBootApplication
//开启事务管理器
@EnableTransactionManagement
@Slf4j
@MapperScan("com.swqsv.cofeeol.mapper")
public class CoffeeolApplication implements ApplicationRunner {

    @Autowired
    private CoffeeMapper coffeeMapper;

    public static void main(String[] args) {
        SpringApplication.run(CoffeeolApplication.class, args);
    }

    @Override
    //开启事务
    @Transactional
    public void run(ApplicationArguments args) throws Exception {
        //generateArtifacts();
        coffeeMapper.findAllWithRowBounds(new RowBounds(1, 10))
                .forEach(c -> log.info("Page(1) Coffee {}", c));
        // coffeeMapper.findAllWithRowBounds(new RowBounds(2, 3))
        //         .forEach(c -> log.info("Page(2) Coffee {}", c));
        //
        // log.info("===================");
        // //如果输入limit0就是所有数据都查出来
        // coffeeMapper.findAllWithRowBounds(new RowBounds(1, 0))
        //         .forEach(c -> log.info("Page(1) Coffee {}", c));
        //
        // log.info("===================");
        //
        // coffeeMapper.findAllWithParam(1, 3)
        //         .forEach(c -> log.info("Page(1) Coffee {}", c));
        // //这个也是查全部数据
        // coffeeMapper.findAllWithParam(1, 0)
        //         .forEach(c -> log.info("Page(1) Coffee {}", c));
        // List<Coffee> list = coffeeMapper.findAllWithParam(2, 3);
        // PageInfo page = new PageInfo(list);
        // log.info("PageInfo: {}", page);

    }

    // private void generateArtifacts() throws Exception {
    //     List<String> warnings = new ArrayList<>();
    //     ConfigurationParser cp = new ConfigurationParser(warnings);
    //     Configuration config = cp.parseConfiguration(
    //             this.getClass().getResourceAsStream("/generatorConfig.xml"));
    //     DefaultShellCallback callback = new DefaultShellCallback(true);
    //     MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
    //     myBatisGenerator.generate(null);
    // }


}
