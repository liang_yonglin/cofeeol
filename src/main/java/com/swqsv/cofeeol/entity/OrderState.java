package com.swqsv.cofeeol.entity;

/**
 * @author duguq
 */

public enum OrderState {
    INIT, PAID, BREWING, BREWED, TAKEN, CANCELLED
}
