package com.swqsv.stock.CandlestickEnum;

/**
 * 日本蜡烛图单根蜡烛枚举
 * 阴使用0来代替,阳使用1来代替
 *
 * @author duguq
 */
public enum SingleShapeEnum {
    /**
     * 大阴线---（开盘价-收盘价）/开盘价>7%
     *
     */
    LONG_DAYS0,
    /**
     * 大阳线---（收盘价-开盘价）/开盘价>7%
     *
     */
    LONG_DAYS1,

    /**
     * 小阴线---3%>（开盘价-收盘价）/开盘价>1%
     */
    SHORT_DAYS0,
    /**
     * 小阳线---3%>（收盘价-开盘价）/开盘价>1%
     */
    SHORT_DAYS1,

    /**
     * 光头光脚 阴 开盘价即成为全日最高价，而收盘价成为全日最低价
     */
    MARUBOZU0,
    /**
     * 光头光脚 阳 开盘价即成为全日最低价，而收盘价成为全日最高价
     */
    MARUBOZU1,

    /**
     * 光头收盘  最高价收盘
     */
    CLOSING_MARUBOZU0,
    /**
     * 光脚收盘 最低价收盘
     */
    CLOSING_MARUBOZU1,

    /**
     * 光脚开盘
     */
    OPENING_MARUBOZU0,

    /**
     * 光头开盘
     */
    OPENING_MARUBOZU1,
    /**
     * 纺锤
     */
    KOMA0,
    /**
     * 纺锤
     */
    KOMA1,
    /**
     * 十字
     */
    DOJI,
    /**
     * 长腿十字
     */
    JUJI,
    /**
     * 墓碑十字
     */
    HAKAISHI
}
