package com.swqsv.stock.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class IStockBaseEntity {
    /**
     * 开盘价
     */
    private BigDecimal openPrice;
    /**
     * 收盘价
     */
    private BigDecimal closePrice;
    /**
     * 最低价
     */
    private BigDecimal minPrice;
    /**
     * 最高价
     */
    private BigDecimal maxPrice;
    /**
     * 成交量
     */
    private BigDecimal volume;


}
