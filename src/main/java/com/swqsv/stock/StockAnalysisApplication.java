package com.swqsv.stock;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author duguq
 */
@SpringBootApplication
//开启事务管理器
@EnableTransactionManagement
@Slf4j
@MapperScan("com.swqsv.cofeeol.mapper")
public class StockAnalysisApplication {
    public static void main(String[] args) {
        SpringApplication.run(StockAnalysisApplication.class, args);
    }


}
