package 享元模式;

public class Test {
    public static void main(String[] args) {
        Integer n1 = Integer.valueOf(50000);
        Integer n2 = Integer.valueOf(50000);
        System.out.println(n1.equals(n2));
        System.out.println(n1 == n2);
    }
}
