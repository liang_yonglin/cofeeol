package 享元模式.基础2;

/**
 * @author duguq
 */
public interface Shape {
    void draw();
}