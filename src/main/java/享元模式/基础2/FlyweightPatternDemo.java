package 享元模式.基础2;

/**
 * 有限范围内尽量重复使用同一个对象,如果有无数种外部状态那么享元模式也没有作用了.
 *
 * @author duguq
 */
public class FlyweightPatternDemo {
    private static final String[] colors =
            {"Red", "Green", "Blue", "White", "Black"};
    // 这里是不可共享的状态,所以这里是外部状态,这里就是说同一个圆,可以有5种的颜色,但是也可以有7种,但是如果能使用rbg就有无数种了

    public static void main(String[] args) {

        for (int i = 0; i < 20; ++i) {
            Circle circle =
                    (Circle) ShapeFactory.getCircle(getRandomColor());
            circle.setX(getRandomX());
            circle.setY(getRandomY());
            circle.setRadius(100);
            circle.draw();
        }
    }

    private static String getRandomColor() {
        return colors[(int) (Math.random() * colors.length)];
    }

    private static int getRandomX() {
        return (int) (Math.random() * 100);
    }

    private static int getRandomY() {
        return (int) (Math.random() * 100);
    }
}