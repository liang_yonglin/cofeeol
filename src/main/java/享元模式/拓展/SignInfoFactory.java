package 享元模式.拓展;

import java.util.HashMap;

/**
 * 享元工厂
 *
 * @author duguq
 */
public class SignInfoFactory {
    //池容器
    private static final HashMap<ExtrinsicState, SignInfo> pool = new HashMap<>();
    private static final HashMap<String, SignInfo> pool1 = new HashMap<>();

    //从池中获得对象
    public static SignInfo getSignInfo(ExtrinsicState key) {
        //设置返回对象
        SignInfo result = null;
        //池中没有该对象，则建立，并放入池中
        if (!pool.containsKey(key)) {
            result = new SignInfo();
            pool.put(key, result);
        } else {
            result = pool.get(key);
        }
        return result;
    }

    public static SignInfo getSignInfo(String key) {
        //设置返回对象
        SignInfo result = null;
        //池中没有该对象，则建立，并放入池中
        if (!pool1.containsKey(key)) {
            result = new SignInfo();
            pool1.put(key, result);
        } else {
            result = pool1.get(key);
        }
        return result;
    }
}