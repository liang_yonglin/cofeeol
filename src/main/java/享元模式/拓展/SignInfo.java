package 享元模式.拓展;

/**
 * 报考信息
 *
 * @author duguq
 */
public class SignInfo {
    /**
     * 报名人员的ID
     */
    private String id;
    /**
     * 考试地点
     */
    private String location;

    /**
     * 外部状态类
     */
    private ExtrinsicState extrinsicState;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public ExtrinsicState getExtrinsicState() {
        return extrinsicState;
    }

    public void setExtrinsicState(ExtrinsicState extrinsicState) {
        this.extrinsicState = extrinsicState;
    }
}