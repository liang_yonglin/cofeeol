package 中介者模式;

/**
 * 抽象协作类
 */
public abstract class AbstractColleague {
    protected AbstractMediator mediator;

    /**
     * 指定中介
     *
     * @param _mediator
     */
    public AbstractColleague(AbstractMediator _mediator) {
        this.mediator = _mediator;
    }
}