package 中介者模式.自我练习协作包;

import 中介者模式.自我练习.中介者包.ShopPlatform;

public class SellerColleague extends Colleague {
    public SellerColleague(ShopPlatform shopPlatform) {
        super(shopPlatform);
    }

    /**
     * 自有方法,处理自己的业务逻辑
     */
    public void sellShop(int number) {
        //使用中介去卖东西
        shopPlatform.execute("sell", number);
    }

    public void offSale() {
        shopPlatform.execute("offSell");
    }
}
