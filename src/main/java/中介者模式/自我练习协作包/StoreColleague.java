package 中介者模式.自我练习协作包;

import 中介者模式.自我练习.中介者包.ShopPlatform;

public class StoreColleague extends Colleague {
    /**
     * 刚开始有100个货物
     */
    private static int GOODS_NUMBER = 100;//这个是一个变数

    public StoreColleague(ShopPlatform shopPlatform) {
        super(shopPlatform);
    }

    /**
     * 库存增加
     *
     * @param number
     */
    public void increase(int number) {
        GOODS_NUMBER = GOODS_NUMBER + number;
        System.out.println("库存数量为：" + GOODS_NUMBER);
    }

    /**
     * 库存降低
     *
     * @param number
     */
    public void decrease(int number) {
        GOODS_NUMBER = GOODS_NUMBER - number;
        System.out.println("库存数量为：" + GOODS_NUMBER);
    }

    /**
     * 获得库存数量
     *
     * @return
     */
    public int getStockNumber() {
        return GOODS_NUMBER;
    }

    /**
     * 存货压力大了，就要通知采购人员不要采购，销售人员要尽快销售
     */
    public void clearStock() {
        super.shopPlatform.execute("clearStore");
    }
}
