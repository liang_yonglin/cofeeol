package 中介者模式.自我练习协作包;

import 中介者模式.自我练习.中介者包.ShopPlatform;

public class ShopperColleague extends Colleague {
    public ShopperColleague(ShopPlatform shopPlatform) {
        super(shopPlatform);
    }


    /**
     * 自身逻辑,开始进货
     *
     * @param object
     */
    public void shop(Integer object) {
        shopPlatform.execute("shop", object);
    }

    /**
     * 自身逻辑,停止进货
     */
    public void stopShop() {
        shopPlatform.execute("stopShop");
    }

}
