package 中介者模式.自我练习协作包;

import 中介者模式.自我练习.中介者包.ShopPlatform;

public abstract class Colleague {
    protected ShopPlatform shopPlatform;

    public Colleague(ShopPlatform shopPlatform) {
        this.shopPlatform = shopPlatform;
    }
}
