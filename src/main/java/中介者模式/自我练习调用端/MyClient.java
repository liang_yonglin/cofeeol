package 中介者模式.自我练习调用端;

import 中介者模式.自我练习.中介者包.JdShopPlatform;
import 中介者模式.自我练习.中介者包.ShopPlatform;
import 中介者模式.自我练习协作包.SellerColleague;
import 中介者模式.自我练习协作包.ShopperColleague;
import 中介者模式.自我练习协作包.StoreColleague;

public class MyClient {
    public static void main(String[] args) {
        //创建一个电商平台,这里使用的是京东平台
        ShopPlatform platform = new JdShopPlatform();
        //采购员
        ShopperColleague shopperColleague = new ShopperColleague(platform);
        shopperColleague.shop(100);//买100个商品
        //销售员
        SellerColleague sellerColleague = new SellerColleague(platform);
        sellerColleague.sellShop(1);
        //库存
        StoreColleague storeColleague = new StoreColleague(platform);
        storeColleague.clearStock();
    }
}
