package 中介者模式.自我练习.中介者包;

public class JdShopPlatform extends ShopPlatform {
    /**
     * 执行业务操作,这些方法都是通过中介完成的
     */
    @Override
    public void execute(String action, Object... objects) {
        switch (action) {
            case "shop":
                shopGoods((Integer) objects[0]);
                break;
            case "sell":
                sellGoods((Integer) objects[0]);
                break;
            case "offSell"://折价销售
                offSellGoods();
                break;
            case "clearStore"://清仓
                clearStore();
                break;
            case "stopShop":
                stopShop();
                break;
            default:
                break;
        }
    }

    private void stopShop() {
        System.out.println("停止进货");
    }

    /**
     * 清仓商品
     */
    private void clearStore() {
        //要求清仓销售
        super.sellerColleague.offSale();
        //要求采购人员不要采购
        super.shopperColleague.stopShop();
    }

    /**
     * 折扣销售商品
     */
    private void offSellGoods() {
        System.out.println("折价销售" + storeColleague.getStockNumber() + "个商品");
    }

    private void sellGoods(Integer object) {
        if (super.storeColleague.getStockNumber() < object) {  //库存数量不够销售,
            super.shopperColleague.shop(object);//简单一点了,直接采购等量的新品
        }
        super.storeColleague.decrease(object);
    }

    /**
     * 采购商品而已
     *
     * @param object
     */
    private void shopGoods(Integer object) {
        if (storeColleague.getStockNumber() < 500) { //进货,如果库存量小于500的时候就进货
            System.out.println("给我采购" + object + "个商品");
            storeColleague.increase(object);
        } else {//折半采购
            int buyNumber = object / 2;
            shopperColleague.shop(buyNumber);
        }
    }


}
