package 中介者模式.自我练习.中介者包;

import 中介者模式.自我练习协作包.SellerColleague;
import 中介者模式.自我练习协作包.ShopperColleague;
import 中介者模式.自我练习协作包.StoreColleague;

public abstract class ShopPlatform {
    //中介当然要知道所有协作类的东西啦,不然怎么做事?
    protected SellerColleague sellerColleague;
    protected ShopperColleague shopperColleague;
    protected StoreColleague storeColleague;

    public ShopPlatform() {//初始化平台的时候要把对应的同事类给初始化出来
        sellerColleague = new SellerColleague(this);
        shopperColleague = new ShopperColleague(this);
        storeColleague = new StoreColleague(this);
    }

    /**
     * 执行业务操作
     */
    public abstract void execute(String action, Object... objects);
}
