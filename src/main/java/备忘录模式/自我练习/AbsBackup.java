package 备忘录模式.自我练习;

import lombok.Data;

import java.util.Date;

/**
 * @author duguq
 */
@Data
public class AbsBackup {
    /**
     * 状态
     */
    private String state;
    /**
     * 内容
     */
    private String content;

    /**
     * 备份名称
     */
    private String name;
    /**
     * 备份时间
     */
    private Date createTime;
}
