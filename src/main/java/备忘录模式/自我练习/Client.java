package 备忘录模式.自我练习;

/**
 * 构思,制作一个带有检查点的状态还原,类似win10的还原点,或者说编辑邮件时候的还原点
 *
 * @author duguq
 */
public class Client {
    public static void main(String[] args) {
        //作为发起人
        AbsBackup backup = new SystemBackupImpl();
        //作为管理者
        RestorePoint restorePoint = new RestorePoint();
        backup.setState("a");
        backup.setContent("创建了第一行");
        backup.setName("第一个还原点");
        restorePoint.createBackup(backup);// 第一次存
        backup.setState("b");
        backup.setContent("创建了第一行和第二行");
        backup.setName("第二个还原点");
        restorePoint.createBackup(backup);// 第二次存
        // ----------设置完毕-----------
        // 恢复备份
        AbsBackup a = restorePoint.restoreBackup("第一个还原点");
        System.out.println(a.toString());

    }
}
