package 备忘录模式.自我练习;

import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 还原点,用来管理备份的
 *
 * @author duguq
 */
@Data
public class RestorePoint {
    /**
     * 放到内存里面去了
     */
    private Map<String, AbsBackup> backMap = new HashMap<>();
    private AbsBackup backup;

    /**
     * 创建还原点
     */
    public void createBackup(AbsBackup backup) {
        AbsBackup systemBackup = new SystemBackupImpl();
        BeanUtils.copyProperties(backup, systemBackup);
        backMap.put(backup.getName(), systemBackup);
    }


    /**
     * 根据还原点名称来进行恢复
     *
     * @param name 还原点名称
     * @return
     */
    public AbsBackup restoreBackup(String name) {
        return backMap.get(name);
    }
}
