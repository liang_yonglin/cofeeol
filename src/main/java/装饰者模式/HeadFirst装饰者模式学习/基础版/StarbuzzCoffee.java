package 装饰者模式.HeadFirst装饰者模式学习.基础版;

/**
 * @author swqsv
 * @date 2022/10/7 20:55
 */
public class StarbuzzCoffee {
    public static void main(String[] args) {
        // 一杯浓缩咖啡,啥也不加
        Beverage espresso = new Espresso();

        System.out.println("espresso.getDescription() = " + espresso.getDescription() + " $" + espresso.cost());


        // 整一杯深度烘培咖啡,加两次摩卡和一次奶盖
        Beverage darkRoast = new DarkRoast();
        darkRoast = new Mocha(darkRoast);
        darkRoast = new Mocha(darkRoast);
        darkRoast = new Whip(darkRoast);

        System.out.println("darkRoast.getDescription() = " + darkRoast.getDescription() + " $" + darkRoast.cost());

        // 整一杯混合咖啡，豆奶，摩卡，奶盖都来一份
        Beverage houseBlend = new HouseBlend();
        houseBlend = new Soy(houseBlend);
        houseBlend = new Mocha(houseBlend);
        houseBlend = new Whip(houseBlend);

        System.out.println("houseBlend.getDescription() = " + houseBlend.getDescription() + " $" + houseBlend.cost());
    }
}
