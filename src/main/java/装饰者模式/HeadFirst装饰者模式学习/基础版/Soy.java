package 装饰者模式.HeadFirst装饰者模式学习.基础版;

/**
 * 豆奶
 *
 * @author liangyonglin
 */
public class Soy extends CondimentDecorator {
    public Soy(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public double cost() {
        return beverage.cost() + 0.15;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ",加豆奶";
    }
}
