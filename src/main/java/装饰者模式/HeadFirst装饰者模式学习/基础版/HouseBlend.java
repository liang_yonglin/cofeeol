package 装饰者模式.HeadFirst装饰者模式学习.基础版;

/**
 * 混合咖啡
 *
 * @author liangyonglin
 */
public class HouseBlend extends Beverage {

    public HouseBlend() {
        description = "House Blend Coffee";
    }

    @Override
    public double cost() {
        return 0.89;
    }
}
