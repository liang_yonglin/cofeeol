package 装饰者模式.HeadFirst装饰者模式学习.基础版;

/**
 * 深度烘培
 *
 * @author liangyonglin
 */
public class DarkRoast extends Beverage {

    public DarkRoast() {
        description = "DarkRoast";
    }

    @Override
    public double cost() {
        return 0.99;
    }
}
