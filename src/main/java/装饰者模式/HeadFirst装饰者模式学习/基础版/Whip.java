package 装饰者模式.HeadFirst装饰者模式学习.基础版;

/**
 * 奶盖
 *
 * @author swqsv
 * @date 2022/10/7 20:42
 */
public class Whip extends CondimentDecorator {
    public Whip(Beverage beverage) {
        this.beverage = beverage;
    }


    @Override
    public double cost() {
        return beverage.cost() + 0.10;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ",加奶盖";
    }
}
