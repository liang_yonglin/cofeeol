package 装饰者模式.HeadFirst装饰者模式学习.基础版;

/**
 * 饮料
 * @author liangyonglin
 */
public abstract class Beverage {
    String description = "Unknown Beverage";

    public String getDescription() {
        return description;
    }

    /**
     * 计算费用
     *
     * @return
     */
    public abstract double cost();
}
