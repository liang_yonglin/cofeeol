package 装饰者模式.HeadFirst装饰者模式学习.进阶带容量带解析;

/**
 * @author swqsv
 * @date 2022/10/7 20:55
 */
public class StarbuzzCoffee {
    public static void main(String[] args) {
        // 整一杯混合咖啡，豆奶，摩卡，奶盖都来一份;并且解析添加了的调料
        Beverage houseBlend = new HouseBlend();
        houseBlend.setSize(Size.VENTI);
        houseBlend = new Soy(houseBlend);
        houseBlend = new Mocha(houseBlend);
        houseBlend = new Whip(houseBlend);
        System.out.println(houseBlend.getDescription()
                + " $" + String.format("%.2f", houseBlend.cost()));
    }
}
