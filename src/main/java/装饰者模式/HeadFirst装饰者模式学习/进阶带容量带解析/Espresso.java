package 装饰者模式.HeadFirst装饰者模式学习.进阶带容量带解析;

/**
 * 浓缩咖啡
 *
 * @author liangyonglin
 */
public class Espresso extends Beverage {

    public Espresso() {
        description.add("Espresso");
    }

    /**
     * 浓缩咖啡价格
     *
     * @return
     */
    @Override
    public double cost() {
        return 1.99;
    }
}
