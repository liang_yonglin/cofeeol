package 装饰者模式.HeadFirst装饰者模式学习.进阶带容量带解析;

/**
 * 容量
 *
 * @author swqsv
 * @date 2022/10/7 21:51
 */
public enum Size {
    /**
     * 小杯
     */
    TALL,
    /**
     * 中杯
     */
    GRANDE,
    /**
     * 大杯
     */
    VENTI;
}
