package 装饰者模式.HeadFirst装饰者模式学习.进阶带容量带解析;

import java.util.List;

/**
 * 豆奶
 *
 * @author liangyonglin
 */
public class Soy extends CondimentDecorator {
    public Soy(Beverage beverage) {
        this.beverage = beverage;
        beverage.getDescription().add("Soy");
    }

    @Override
    public double cost() {
        double cost = beverage.cost();
        if (beverage.getSize() == Size.TALL) {
            cost += 0.10;
        } else if (beverage.getSize() == Size.GRANDE) {
            cost += 0.15;
        } else if (beverage.getSize() == Size.VENTI) {
            cost += 0.20;
        }
        return cost;
    }

    @Override
    public List<String> getDescription() {
        return beverage.getDescription();
    }
}
