package 装饰者模式.HeadFirst装饰者模式学习.进阶带容量带解析;

import java.util.List;

/**
 * 奶盖
 *
 * @author swqsv
 * @date 2022/10/7 20:42
 */
public class Whip extends CondimentDecorator {
    public Whip(Beverage beverage) {
        this.beverage = beverage;
        beverage.getDescription().add("Whip");
    }


    @Override
    public double cost() {
        return beverage.cost() + 0.10;
    }

    @Override
    public List<String> getDescription() {
        return beverage.getDescription();
    }
}
