package 装饰者模式.HeadFirst装饰者模式学习.进阶带容量带解析;

import java.util.ArrayList;
import java.util.List;

/**
 * 饮料
 * <p>
 * 依据容量收费,如果是小杯,中杯,大杯分别收取不同的费用
 *
 * @author liangyonglin
 */
public abstract class Beverage {
    /**
     * 饮料的描述
     */
    List<String> description = new ArrayList<>();
    /**
     * 如果不声明,默认就是小杯
     */
    Size size = Size.TALL;

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public List<String> getDescription() {
        return description;
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }

    /**
     * 计算费用
     *
     * @return
     */
    public abstract double cost();
}