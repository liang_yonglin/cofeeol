package 装饰者模式.HeadFirst装饰者模式学习.进阶带容量带解析;

import java.util.List;

/**
 * 摩卡
 *
 * @author liangyonglin
 */
public class Mocha extends CondimentDecorator {

    public Mocha(Beverage beverage) {
        this.beverage = beverage;
        beverage.getDescription().add("Mocha");
    }

    @Override
    public double cost() {
        return beverage.cost() + 0.20;
    }

    @Override
    public List<String> getDescription() {
        // 这里就体现了委托关系,委托给beverage获取了正在装饰对象的描述,然后把装饰的东西附在后面
        return beverage.getDescription();
    }
}
