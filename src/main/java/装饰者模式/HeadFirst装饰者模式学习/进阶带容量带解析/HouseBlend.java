package 装饰者模式.HeadFirst装饰者模式学习.进阶带容量带解析;

/**
 * 混合咖啡
 *
 * @author liangyonglin
 */
public class HouseBlend extends Beverage {

    public HouseBlend() {
        description.add("House Blend Coffee");
    }

    @Override
    public double cost() {
        return 0.89;
    }
}
