package 装饰者模式.HeadFirst装饰者模式学习.进阶带容量;

/**
 * 脱咖啡因
 *
 * @author liangyonglin
 */
public class Decaf extends Beverage {

    public Decaf() {
        description = "Decaf";
    }

    @Override
    public double cost() {
        return 1.05;
    }
}
