package 装饰者模式.HeadFirst装饰者模式学习.进阶带容量;

/**
 * 饮料装饰者
 * <p>
 * 装饰者可能也是被装饰者,所以需要扩展Beverage类
 * @author liangyonglin
 */
public abstract class CondimentDecorator extends Beverage {

    /**
     * 这里需要通过组合持有一个饮料的引用
     * <p>
     * 也是装饰者可能也是被装饰者的体现
     */
    Beverage beverage;

    /**
     * 获取对应的描述
     *
     * @return
     */
    public abstract String getDescription();
}
