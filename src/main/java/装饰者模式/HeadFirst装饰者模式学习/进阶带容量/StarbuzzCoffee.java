package 装饰者模式.HeadFirst装饰者模式学习.进阶带容量;

/**
 * @author swqsv
 * @date 2022/10/7 20:55
 */
public class StarbuzzCoffee {
    public static void main(String[] args) {
        // 一杯浓缩咖啡,啥也不加
        Beverage espresso = new Espresso();

        System.out.println("espresso.getDescription() = " + espresso.getDescription() + " $" + espresso.cost());


        // 整一杯深度烘培咖啡,加两次摩卡和一次奶盖
        Beverage darkRoast = new DarkRoast();
        darkRoast = new Mocha(darkRoast);
        darkRoast = new Mocha(darkRoast);
        darkRoast = new Whip(darkRoast);

        System.out.println("darkRoast.getDescription() = " + darkRoast.getDescription() + " $" + darkRoast.cost());

        // 整一杯混合咖啡，豆奶，摩卡，奶盖都来一份
        Beverage houseBlend = new HouseBlend();
        houseBlend.setSize(Size.VENTI);
        houseBlend = new Soy(houseBlend);
        houseBlend = new Mocha(houseBlend);
        houseBlend = new Whip(houseBlend);

        // 两个double相加精度会不准确
        System.out.println("houseBlend.getDescription() = " + houseBlend.getDescription() + " $" + houseBlend.cost());

        System.out.println(houseBlend.getDescription()
                + " $" + String.format("%.2f", houseBlend.cost()));
    }
}
