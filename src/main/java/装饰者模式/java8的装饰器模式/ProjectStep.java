package 装饰者模式.java8的装饰器模式;

/**
 * @author swqsv
 * @date 2022/8/7 22:22
 */
public class ProjectStep {
    String saveHeader(String json) {
        System.out.println("ProjectStep.saveHeader");
        return json;
    }



    String saveExplain(String json) {
        System.out.println("ProjectStep.saveExplain");
        return json;
    }


    public ProjectStep() {
    }
}
