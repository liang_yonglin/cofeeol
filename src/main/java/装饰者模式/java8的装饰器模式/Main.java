package 装饰者模式.java8的装饰器模式;

import java.util.function.Function;

/**
 * @author swqsv
 * @date 2022/8/7 22:24
 */
public class Main {
    public static void main(String[] args) {
//        Function<String,Function<ProjectStep,Function<ProjectStepSaveExtExplain,ProjectStepSaveTaxInfo>>> extTaxInfo =
//                s -> (Function<ProjectStep, Function<ProjectStepSaveExtExplain, ProjectStepSaveTaxInfo>>) projectStep
//                        -> (Function<ProjectStepSaveExtExplain, ProjectStepSaveTaxInfo>) projectStepSaveExtExplain
//                        -> new ProjectStepSaveTaxInfo();
//        Function<String,Function<ProjectStep,ProjectStepSaveExtExplain>> extExplain = s -> (Function<ProjectStep, ProjectStepSaveExtExplain>) projectStep -> new ProjectStepSaveExtExplain();
//        Function<String, ProjectStep> baseStep = s -> new ProjectStep();
//        ProjectStepSaveTaxInfo stepSaveTaxInfo = extTaxInfo.apply("我这么靓仔").apply(new ProjectStep()).apply(new ProjectStepSaveExtExplain());
//        stepSaveTaxInfo.saveTaxInfo();
        ProjectStep projectStep = new ProjectStep();
        Function<String,String> base = projectStep::saveHeader;
        Function<String,String> explain = base.andThen(projectStep::saveExplain);
        ProjectStepSaveExtExplain p = new ProjectStepSaveExtExplain();
        Function<String,String> explain2 = explain.andThen(p::saveExtExplain);
        ProjectStepSaveTaxInfo p2 = new ProjectStepSaveTaxInfo();
        Function<String,Boolean> explain3 = explain2.andThen(p2::saveTaxInfo);
        System.out.println(explain3.apply("a项目"));
    }
}
