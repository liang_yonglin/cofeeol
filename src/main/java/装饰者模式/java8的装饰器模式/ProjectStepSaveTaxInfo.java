package 装饰者模式.java8的装饰器模式;

/**
 * @author swqsv
 * @date 2022/8/7 22:25
 */
public class ProjectStepSaveTaxInfo extends ProjectStep {

    private ProjectStep projectStep;

    public ProjectStepSaveTaxInfo(ProjectStep projectStep) {
        this.projectStep = projectStep;
    }

    public ProjectStepSaveTaxInfo() {
    }

    boolean saveTaxInfo(String json) {
        System.out.println("ProjectStep.saveTaxInfo");
        return true;
    }

}
