package 装饰者模式.java8的装饰器模式;

/**
 * @author swqsv
 * @date 2022/8/7 22:25
 */
public class ProjectStepSaveExtExplain extends ProjectStep {

    private ProjectStep projectStep;

    public ProjectStepSaveExtExplain(ProjectStep projectStep) {
        this.projectStep = projectStep;
    }

    public ProjectStepSaveExtExplain() {
    }

    String saveExtExplain(String json) {
        System.out.println("ProjectStep.saveExtExplain");
        return json;
    }


}
