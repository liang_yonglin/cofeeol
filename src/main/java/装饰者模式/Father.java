package 装饰者模式;

import 装饰者模式.装饰.HighScoreDecorator;
import 装饰者模式.装饰.SortDecorator;

public class Father {
    public static void main(String[] args) {
        //把成绩单拿过来
        SchoolReport sr;
        //又加了成绩排名的说明     加了最高分说明的成绩单;   原装的成绩单
        sr = new SortDecorator(new HighScoreDecorator(new FouthGradeSchoolReport()));
        //看成绩单
        sr.report();
        //然后老爸一看，很开心，就签名了
        sr.sign("老三");  //我叫小三，老爸当然叫老三
    }
}