package 装饰者模式.java8装饰器实现2;

import java.util.function.Function;

/**
 * @author swqsv
 * @date 2022/8/7 23:26
 */
public interface FunctionPlus extends Function<String, Boolean> {

    default FunctionPlus next(Function<String, ? extends Boolean> after) {
        return (String t) -> apply(t) && after.apply(t);
    }

}
