package 装饰者模式.java8装饰器实现2;

import com.google.common.collect.Lists;

import java.util.ArrayList;

/**
 * @author swqsv
 * @date 2022/8/7 22:24
 */
public class Main {
    public static void main(String[] args) {
        ProjectStep projectStep = new ProjectStep();
        FunctionPlus base = projectStep::saveHeader;

        FunctionPlus explain = base.next(projectStep::saveExplain);

        ArrayList<Object> objects = Lists.newArrayList();
        
//        int sum = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9).reduce(0, (acc, n) -> acc + n);
//        Boolean aaa = explain.apply("aaa");
//
//        ProjectStepSaveExtExplain p = new ProjectStepSaveExtExplain();
//        Function<String,String> explain2 = explain.andThen(p::saveExtExplain);
//
//        ProjectStepSaveTaxInfo p2 = new ProjectStepSaveTaxInfo();
//        Function<String,Boolean> explain3 = explain2.andThen(p2::saveTaxInfo);
//
//        System.out.println(explain3.apply("a项目"));
    }
}
