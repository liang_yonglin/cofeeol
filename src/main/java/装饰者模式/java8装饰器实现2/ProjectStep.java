package 装饰者模式.java8装饰器实现2;

/**
 * @author swqsv
 * @date 2022/8/7 22:22
 */
public class ProjectStep {
    boolean saveHeader(String json) {
        System.out.println("ProjectStep.saveHeader");
        return true;
    }



    boolean saveExplain(String json) {
        System.out.println("ProjectStep.saveExplain");
        return true;
    }
    boolean saveTaxInfo(String json) {
        System.out.println("ProjectStep.saveTaxInfo");
        return true;
    }
    boolean saveExtExplain(String json) {
        System.out.println("ProjectStep.saveExtExplain");
        return true;
    }

    public ProjectStep() {
    }
}
