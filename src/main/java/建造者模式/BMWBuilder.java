package 建造者模式;

import java.util.ArrayList;

public class BMWBuilder extends CarBuilder {
    private BMWModel bmw = new BMWModel();

    public CarModel getCarModel() {
        return bmw;
    }

    public void setSequence(ArrayList<String> sequence) {
        bmw.setSequence(sequence);
    }
}