package 适配器模式.扩展.外包人员信息;

import java.util.Map;

public interface IOuterUserBaseInfo {
    //基本信息，比如名称、性别、手机号码等
    Map getUserBaseInfo();
}