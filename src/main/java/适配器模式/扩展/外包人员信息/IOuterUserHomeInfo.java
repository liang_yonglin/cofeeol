package 适配器模式.扩展.外包人员信息;

import java.util.Map;

public interface IOuterUserHomeInfo {
    //用户的家庭信息
    Map getUserHomeInfo();
}