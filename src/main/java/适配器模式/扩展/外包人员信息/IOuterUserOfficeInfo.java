package 适配器模式.扩展.外包人员信息;

import java.util.Map;

public interface IOuterUserOfficeInfo {
    //工作区域信息
    Map getUserOfficeInfo();
}