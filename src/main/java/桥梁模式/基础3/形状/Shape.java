package 桥梁模式.基础3.形状;

import 桥梁模式.基础3.颜色.Color;

public abstract class Shape {
    Color color;

    public void setColor(Color color) {
        this.color = color;
    }

    public abstract void draw();
}