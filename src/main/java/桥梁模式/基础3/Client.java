package 桥梁模式.基础3;

import 桥梁模式.基础3.形状.Rectangle;
import 桥梁模式.基础3.形状.Shape;
import 桥梁模式.基础3.形状.Square;
import 桥梁模式.基础3.颜色.Color;
import 桥梁模式.基础3.颜色.White;

public class Client {
    public static void main(String[] args) {
        //白色
        Color white = new White();
        //正方形
        Shape square = new Square();
        //白色的正方形
        square.setColor(white);
        square.draw();

        //长方形
        Shape rectange = new Rectangle();
        rectange.setColor(white);
        rectange.draw();
    }
}