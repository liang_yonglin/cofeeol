package 桥梁模式.基础2.公司;

import 桥梁模式.基础2.产品.Product;

/**
 * @author duguq
 */
public abstract class Corp {
    //定义一个抽象的产品对象，不知道具体是什么产品
    private final Product product;

    //构造函数，由子类定义传递具体的产品进来
    public Corp(Product product) { // 这样就可以直接通过构造函数来分辨出需要制造哪一个产品了
        this.product = product;
    }

    //公司是干什么的？赚钱的！
    public void makeMoney() {
        //每家公司都是一样，先生产
        this.product.beProducted();
        //然后销售
        this.product.beSelled();
    }
}