package 桥梁模式.基础2.公司;

import 桥梁模式.基础2.产品.House;

public class HouseCorp extends Corp {
    //定义传递一个House产品进来
    public HouseCorp(House house) {
        super(house);
    }

    //房地产公司很High了，赚钱，计算利润
    @Override
    public void makeMoney() {
        super.makeMoney();
        System.out.println("房地产公司赚大钱了...");
    }
}