package 游戏后台;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author swqsv
 * @date 2022/9/12 18:03
 */
@NoArgsConstructor
@Data
public class PropItem {
    private Integer code;
    private List<ReqDataDTO> reqData;
    private String msg;

    @NoArgsConstructor
    @Data
    public static class ReqDataDTO {
        private String itemId;
        private String itemName;
    }
}
