package 游戏后台;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.entity.mime.MultipartEntityBuilder;
import org.apache.hc.client5.http.entity.mime.StringBody;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.entity.EntityUtils;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.apache.hc.core5.http.ContentType.MULTIPART_FORM_DATA;

/**
 * @author swqsv
 * @date 2022/9/11 15:56
 */
public class GameProp {
    static final CloseableHttpClient httpclient = HttpClients.createDefault();
    public static int count = 1;

    public static void main(String[] args) {
        List<CommonProp> propList = Lists.newArrayList();

        for (int i = 0; i < 20; i++) {
            propList.add(new CommonProp("27014", "1000"));
        }

        for (CommonProp commonProp : propList) {
            sendSingleProp(commonProp, 1L);
        }

    }

    private static void sendSingleProp(CommonProp commonProp, long targetNum) {
        String num = commonProp.getPreNum();
        int sendPreNum = Integer.parseInt(num);
        do {
            sendProp(commonProp);
            targetNum -= sendPreNum;
        } while (targetNum > 0);
    }

    private static void sendProp(CommonProp commonProp) {
        // 创建一个会自动关闭的httpclient
        final HttpPost httppost = new HttpPost("http://api.fannjie.com/index/sendItem/data/" +
                "YzFjZFE1RTE2YSs1VGdSZVd6c3dWd1IwU3JiUWhlRmpYY0xNUkVZbXFING15WFdZeVpncFJPdEQxbG1uZHpBbjRyNldOTDhmMjF0eUNWTlVLcWhlbHR6NVlnYkhCemJCY1JPQkFEcVJWTEdQZFhNVDBxc01ZbVVBVjFTb3JvSHk=");

        // 设置post参数
        final HttpEntity reqEntity = MultipartEntityBuilder.create()
                .addPart("itemId", new StringBody(commonProp.getName(), MULTIPART_FORM_DATA))
                .addPart("itemNum", new StringBody(commonProp.getPreNum(), MULTIPART_FORM_DATA))
                .addPart("itemType", new StringBody("item", MULTIPART_FORM_DATA))
                .addPart("serverId", new StringBody("1", MULTIPART_FORM_DATA))
                .build();
        httppost.setEntity(reqEntity);
        try (final CloseableHttpResponse response = httpclient.execute(httppost)) {
            final HttpEntity resEntity = response.getEntity();
            String responseStr = EntityUtils.toString(resEntity);
            CommonResp commonResp = new Gson().fromJson(responseStr, CommonResp.class);
            if (commonResp.getCode() == 0) {
                System.out.println("第" + count + "次道具发送成功咯");
                count++;
            }
            TimeUnit.SECONDS.sleep(10);
        } catch (ParseException | InterruptedException | IOException e) {
            throw new RuntimeException(e);
        }
    }
}
