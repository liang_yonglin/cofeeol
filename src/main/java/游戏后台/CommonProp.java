package 游戏后台;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author swqsv
 * @date 2022/9/11 15:08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommonProp {
    /**
     * 道具名称
     */
    private String name;
    /**
     * 单词发送道具数量
     */
    private String preNum;
}
