package 游戏后台;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author swqsv
 * @date 2022/9/11 16:15
 */
@NoArgsConstructor
@Data
public class CommonResp {

    /**
     * 返回代码,0:成功
     */
    private Integer code;
    private List<?> reqData;
    private String msg;
}
