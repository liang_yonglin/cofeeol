package 责任链模式.自我练习.客户端;

import 责任链模式.自我练习.处理人.Boss;
import 责任链模式.自我练习.处理人.GroupLeader;
import 责任链模式.自我练习.处理人.Handler;
import 责任链模式.自我练习.处理人.Manager;
import 责任链模式.自我练习.配置.Request;
import 责任链模式.自我练习.配置.Response;

public class Client {
    public static void main(String[] args) {
        //声明所有的处理节点
        Handler handler1 = new GroupLeader();
        Handler handler2 = new Manager();
        Handler handler3 = new Boss();
        //设置链中的阶段顺序1-->2-->3 ; 老板最大,3天假期需要老板审批
        handler1.setNextHandler(handler2);
        handler2.setNextHandler(handler3);
        //提交请求，返回结果
        Request request = new Request(1);//请一天假期试试水
        Response response = handler1.handleMessage(request);
        System.out.println(response);
    }
}