package 责任链模式.自我练习.配置;

import java.util.StringJoiner;

/**
 * 响应
 */
public class Response {
    /**
     * 审批信息
     */
    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Response.class.getSimpleName() + "[", "]")
                .add("message='" + message + "'")
                .toString();
    }
}