package 责任链模式.自我练习.配置;

/**
 * 请求,看你要请多少天假
 */
public class Request {
    private final Integer num;

    public Request(Integer num) {
        this.num = num;
    }

    //请求的等级
    public Level getRequestLevel() {
        switch (num) {
            case 1:
                return Level.ONE;
            case 2:
                return Level.TWO;
            default:
                return Level.THREE;
        }
    }
}