package 责任链模式.自我练习.配置;

public enum Level {
    /**
     * 一天假
     */
    ONE,
    /**
     * 两天假
     */
    TWO,
    /**
     * 三天假
     */
    THREE
}

