package 责任链模式.自我练习.处理人;

import 责任链模式.自我练习.配置.Level;
import 责任链模式.自我练习.配置.Request;
import 责任链模式.自我练习.配置.Response;

/**
 * 老板
 */
public class Boss extends Handler {
    @Override
    protected Level getHandlerLevel() {
        return Level.THREE;
    }

    @Override
    protected Response echo(Request request) {
        Response response = new Response();
        response.setMessage("老板同意请假");
        return response;
    }
}
