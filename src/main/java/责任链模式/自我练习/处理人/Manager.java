package 责任链模式.自我练习.处理人;

import 责任链模式.自我练习.配置.Level;
import 责任链模式.自我练习.配置.Request;
import 责任链模式.自我练习.配置.Response;

/**
 * 总经理
 */
public class Manager extends Handler {
    @Override
    protected Level getHandlerLevel() {
        return Level.TWO;
    }

    @Override
    protected Response echo(Request request) {
        Response response = new Response();
        response.setMessage("经理同意请假");
        return response;
    }
}
