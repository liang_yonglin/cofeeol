package 责任链模式.自我练习.处理人;

import 责任链模式.自我练习.配置.Level;
import 责任链模式.自我练习.配置.Request;
import 责任链模式.自我练习.配置.Response;

/**
 * 处理人
 */
public abstract class Handler {

    /**
     * 下一个处理者,类似递归调用
     */
    private Handler nextHandler;

    public void setNextHandler(Handler nextHandler) {
        this.nextHandler = nextHandler;
    }

    //每个处理者都有一个处理级别
    protected abstract Level getHandlerLevel();

    //每个处理者都必须实现处理任务
    protected abstract Response echo(Request request);

    public final Response handleMessage(Request request) {
        Response response = null;
        //判断是否是自己的处理级别
        if (this.getHandlerLevel().equals(request.getRequestLevel())) {
            response = this.echo(request);
        } else {//不属于自己的处理级别
            //判断是否有下一个处理者
            if (this.nextHandler != null) {
                response = this.nextHandler.handleMessage(request);
            } else {
                //没有适当的处理者，业务自行处理
                System.out.println("不可能会来到这里,因为枚举已经限制死了");
            }
        }
        return response;
    }

}
