package pattern02_观察者模式.HeadFirst观察者模式学习;

/**
 * 观察者
 *
 * @author swqsv
 * @date 2022/9/4 15:31
 */
public interface Observer {
    /**
     * 更新
     *
     * @param temp     温度
     * @param humidity 湿度
     * @param pressure 压力
     */
    void update(float temp, float humidity, float pressure);
}
