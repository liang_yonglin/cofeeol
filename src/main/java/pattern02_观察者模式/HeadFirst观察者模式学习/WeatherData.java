package pattern02_观察者模式.HeadFirst观察者模式学习;

import java.util.ArrayList;
import java.util.List;

/**
 * 气象数据
 *
 * @author swqsv
 * @date 2022/9/4 15:46
 */
public class WeatherData implements Subject {

    /**
     * 观察者集合
     */
    private List<Observer> observers;

    /**
     * 温度
     */
    private float temperature;

    /**
     * 湿度
     */
    private float humidity;

    /**
     * 压力
     */
    private float pressure;

    /**
     * 在构造方法初始化观察者列表
     */
    public WeatherData() {
        observers = new ArrayList<>();
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update(temperature, humidity, pressure);
        }
    }

    /**
     * 测量数据发生变化
     */
    public void measurementsChanged() {
        notifyObservers();
    }

    /**
     * 设置测量数据
     *
     * @param temperature 温度
     * @param humidity    湿度
     * @param pressure    压力
     */
    public void setMeasurements(float temperature, float humidity, float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
    }

}
