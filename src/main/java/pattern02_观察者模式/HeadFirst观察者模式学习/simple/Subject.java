package pattern02_观察者模式.HeadFirst观察者模式学习.simple;

public interface Subject {
	void registerObserver(Observer o);
	void removeObserver(Observer o);
	void notifyObservers();
}
