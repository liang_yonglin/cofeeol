package pattern02_观察者模式.HeadFirst观察者模式学习.simple;

public interface Observer {
	void update(int value);
}
