package pattern02_观察者模式.HeadFirst观察者模式学习.进阶版可拉取;

/**
 * 观察者
 *
 * @author swqsv
 * @date 2022/9/4 15:31
 */
public interface Observer {
    /**
     * 更新
     *
     */
    void update();
}
