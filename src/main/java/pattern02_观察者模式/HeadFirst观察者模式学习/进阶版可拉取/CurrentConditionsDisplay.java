package pattern02_观察者模式.HeadFirst观察者模式学习.进阶版可拉取;

/**
 * 当前条件展示
 *
 * @author swqsv
 * @date 2022/9/4 16:11
 */
public class CurrentConditionsDisplay implements Observer, DisplayElement {


    /**
     * 温度
     */
    private float temperature;
    /**
     * 湿度
     */
    private float humidity;

    /**
     * 天气主题
     */
    private WeatherData weatherData;

    public CurrentConditionsDisplay(WeatherData weatherData) {
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }

    @Override
    public void display() {
        System.out.println("当前温度：" + temperature + "；" + "当前湿度：" + humidity);
    }

    @Override
    public void update() {
        this.temperature = weatherData.getTemperature();
        this.humidity = weatherData.getHumidity();
        display();
    }
}
