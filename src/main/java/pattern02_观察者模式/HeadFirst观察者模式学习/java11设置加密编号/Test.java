package pattern02_观察者模式.HeadFirst观察者模式学习.java11设置加密编号;

/**
 *
 * @author swqsv
 * @date 2022/9/4 22:27
 */
public class Test {
    public static void main(String[] args) {
        ProjectVO projectVO = new ProjectVO();
        System.out.println("projectVO = " + projectVO);

        projectVO.setProjectId("123");
        System.out.println("projectVO = " + projectVO);

    }
}