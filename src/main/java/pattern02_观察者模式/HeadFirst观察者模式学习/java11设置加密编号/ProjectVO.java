package pattern02_观察者模式.HeadFirst观察者模式学习.java11设置加密编号;

import lombok.Data;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

/**
 * @author swqsv
 * @date 2022/9/4 22:11
 */
@Data
public class ProjectVO {
    /**
     * 未加密的编号
     */
    private String projectId;
    /**
     * 加密字符串
     */
    private String projectIdEn;
    private String projectName;
    private String createDate;
    private String createUser;

    /**
     * 侦听器
     */
    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);


    public ProjectVO() {
        // 订阅了我咁靓仔
        pcs.addPropertyChangeListener("我咁靓仔",new EmailService(this));
        pcs.addPropertyChangeListener("我咁靓仔",new ProjectEnService(this));
    }


    public void setProjectId(String projectId) {
        String oldValue = this.projectId;
        this.projectId = projectId;
        // 发布我咁靓仔事件
        pcs.firePropertyChange("我咁靓仔", oldValue, projectId);
    }

}
