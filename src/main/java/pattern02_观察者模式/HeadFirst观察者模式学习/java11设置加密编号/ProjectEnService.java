package pattern02_观察者模式.HeadFirst观察者模式学习.java11设置加密编号;

import lombok.Data;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;

/**
 * @author swqsv
 * @date 2022/9/4 22:55
 */
@Data
public class ProjectEnService implements PropertyChangeListener, Serializable {

    private ProjectVO projectVO;

    public ProjectEnService(ProjectVO projectVO) {
        this.projectVO = projectVO;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
//        System.out.println("evt = " + evt);
        projectVO.setProjectIdEn(evt.getNewValue() + "abc");
    }
}
