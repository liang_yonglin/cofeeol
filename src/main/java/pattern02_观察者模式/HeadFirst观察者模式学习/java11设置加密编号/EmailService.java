package pattern02_观察者模式.HeadFirst观察者模式学习.java11设置加密编号;

import cn.hutool.core.util.StrUtil;
import lombok.Data;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;

/**
 * @author swqsv
 * @date 2022/9/4 22:51
 */
@Data
public class EmailService implements PropertyChangeListener, Serializable {

    private ProjectVO projectVO;

    public EmailService(ProjectVO projectVO) {
        this.projectVO = projectVO;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (StrUtil.equals(evt.getPropertyName(), "我咁靓仔")) {
            System.out.println("发送邮件给用户，他很靓仔,单据号："+projectVO.getProjectId());
        }
    }
}
