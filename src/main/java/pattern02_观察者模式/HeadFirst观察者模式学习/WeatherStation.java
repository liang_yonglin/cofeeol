package pattern02_观察者模式.HeadFirst观察者模式学习;

/**
 * 气象站
 *
 * @author swqsv
 * @date 2022/9/4 17:20
 */
public class WeatherStation {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();

//        CurrentConditionsDisplay currentConditionsDisplay = new CurrentConditionsDisplay(weatherData);

        StatisticsDisplay statisticsDisplay = new StatisticsDisplay(weatherData);
        ForecastDisplay forecastDisplay = new ForecastDisplay(weatherData);

        weatherData.setMeasurements(80,65,30.4f);
        weatherData.setMeasurements(81,66,30.5f);
        weatherData.setMeasurements(82,67,30.6f);
    }
}
