package pattern02_观察者模式.HeadFirst观察者模式学习;

/**
 * 显示元素
 *
 * @author swqsv
 * @date 2022/9/4 15:34
 */
public interface DisplayElement {
    /**
     * 显示
     */
    void display();
}
