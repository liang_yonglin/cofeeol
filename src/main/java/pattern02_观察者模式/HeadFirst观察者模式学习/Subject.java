package pattern02_观察者模式.HeadFirst观察者模式学习;

/**
 * 主题
 *
 * @author swqsv
 * @date 2022/9/4 15:29
 */
public interface Subject {
    /**
     * 注册观察者
     *
     * @param o
     */
    void registerObserver(Observer o);

    /**
     * 移除观察者
     *
     * @param o
     */
    void removeObserver(Observer o);

    /**
     * 通知观察者
     */
    void notifyObservers();
}