package pattern02_观察者模式.基础使用.观察者;

public interface Observer {
    //一发现别人有动静，自己也要行动起来
    void update(String context);
}