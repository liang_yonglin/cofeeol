package pattern02_观察者模式.基础使用.被观察者;


import pattern02_观察者模式.基础使用.观察者.Observer;

/**
 * 间谍
 */
public interface Observable {
    //增加一个观察者
    void addObserver(Observer observer);

    //删除一个观察者
    void deleteObserver(Observer observer);

    //既然要观察，我发生改变了他也应该有所动作，通知观察者
    void notifyObservers(String context);
}