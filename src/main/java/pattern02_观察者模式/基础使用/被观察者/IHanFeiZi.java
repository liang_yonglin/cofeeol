package pattern02_观察者模式.基础使用.被观察者;

public interface IHanFeiZi {
    //韩非子也是人，也要吃早饭的
    void haveBreakfast();

    //韩非之也是人，是人就要娱乐活动
    void haveFun();
}