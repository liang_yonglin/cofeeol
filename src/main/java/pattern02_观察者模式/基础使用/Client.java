package pattern02_观察者模式.基础使用;

import pattern02_观察者模式.基础使用.被观察者.HanFeiZi;
import pattern02_观察者模式.基础使用.观察者.LiSi;
import pattern02_观察者模式.基础使用.观察者.LiuSi;
import pattern02_观察者模式.基础使用.观察者.Observer;
import pattern02_观察者模式.基础使用.观察者.WangSi;

/**
 * 定义对象间一种一对多的依赖关系，使得每当一个对象改变状态，则所有依赖于它的对象都会得到通知并被自动更新。
 */
public class Client {
    public static void main(String[] args) {
        //三个观察者产生出来
        Observer liSi = new LiSi();
        Observer wangSi = new WangSi();
        Observer liuSi = new LiuSi();
        //定义出韩非子
        HanFeiZi hanFeiZi = new HanFeiZi();
        //我们后人根据历史，描述这个场景，有三个人在观察韩非子
        hanFeiZi.addObserver(liSi);
        hanFeiZi.addObserver(wangSi);
        hanFeiZi.addObserver(liuSi);
        //然后这里我们看看韩非子在干什么
        hanFeiZi.haveBreakfast();
    }
}