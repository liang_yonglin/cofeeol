package pattern02_观察者模式.扩展.java9及之后版本;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Flow;

@Slf4j
public class LiSi implements Flow.Subscriber<HanFeiZi> {
    private Flow.Subscription subscription;

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        this.subscription = subscription;
        System.out.println("订阅成功。。");
        subscription.request(1);
    }

    @Override
    public void onNext(HanFeiZi item) {
        log.info("【onNext 接受到数据 item : {}】 ", item);
    }

    @Override
    public void onError(Throwable throwable) {
        log.info("【onError 出现异常】");
        subscription.cancel();
    }

    @Override
    public void onComplete() {
        log.info("【onComplete 所有数据接收完成】");
    }
}
