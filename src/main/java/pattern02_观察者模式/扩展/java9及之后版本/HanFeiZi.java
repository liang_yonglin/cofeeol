package pattern02_观察者模式.扩展.java9及之后版本;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.concurrent.SubmissionPublisher;

@Data
@ToString
public class HanFeiZi extends SubmissionPublisher<HanFeiZi> {
    private String name;
    private BigDecimal price;

    public void eatLunch() {
        System.out.println("韩非子在吃午饭");
        name = "午饭之名";
    }

    public void sleep() {
        System.out.println("韩非子在睡午觉");
        price = BigDecimal.TEN;
    }
}
