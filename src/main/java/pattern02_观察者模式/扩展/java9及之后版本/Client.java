package pattern02_观察者模式.扩展.java9及之后版本;

import java.util.concurrent.TimeUnit;

public class Client {
    public static void main(String[] args) throws InterruptedException {
        HanFeiZi hanFeiZi = new HanFeiZi();
        LiSi liSi = new LiSi();
        hanFeiZi.subscribe(liSi);
        hanFeiZi.eatLunch();
        hanFeiZi.offer(hanFeiZi, 1, TimeUnit.SECONDS, (subscriber, hanFeiZi1) -> false);
        hanFeiZi.sleep();
        hanFeiZi.submit(hanFeiZi);
        Thread.currentThread().join(100000);
    }
}
