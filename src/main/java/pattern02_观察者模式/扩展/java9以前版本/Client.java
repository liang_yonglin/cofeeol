package pattern02_观察者模式.扩展.java9以前版本;


import java.util.Observer;

/**
 * 用java的工具类,剩下了写观察者和被观察者的接口
 * java9以前用这个方式
 * 有缺陷,不能保证顺序
 */
public class Client {
    public static void main(String[] args) {
        //三个观察者产生出来
        Observer liSi = new LiSi();
        //定义出韩非子
        HanFeiZi hanFeiZi = new HanFeiZi();
        //我们后人根据历史，描述这个场景，有三个人在观察韩非子
        hanFeiZi.addObserver(liSi);
        //然后这里我们看看韩非子在干什么
        hanFeiZi.haveBreakfast();
        hanFeiZi.haveFun();
    }
}