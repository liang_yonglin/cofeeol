package pattern02_观察者模式.扩展.java9以前版本;

import pattern02_观察者模式.基础使用.被观察者.IHanFeiZi;

import java.util.Observable;

/**
 * Observable 用暴露狂来比喻之
 */
public class HanFeiZi extends Observable implements IHanFeiZi {
    //韩非子要吃饭了
    public void haveBreakfast() {
        System.out.println("韩非子：开始吃饭了...");
        //通知所有的观察者
        super.setChanged();
        super.notifyObservers("韩非子在吃饭");
    }

    //韩非子开始娱乐了
    public void haveFun() {
        System.out.println("韩非子：开始娱乐了...");
        super.setChanged();
        this.notifyObservers("韩非子在娱乐");
    }
}