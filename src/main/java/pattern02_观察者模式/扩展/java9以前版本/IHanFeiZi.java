package pattern02_观察者模式.扩展.java9以前版本;

public interface IHanFeiZi {
    //韩非子也是人，也要吃早饭的
    void haveBreakfast();

    //韩非之也是人，是人就要娱乐活动
    void haveFun();
}