package 命令模式;

/**
 * 增加需求的命令
 */
public class AddRequirementCommand extends Command {
    //执行增加一项需求的命令
    public void execute() {
        //找到需求组
        rg.find();
        //增加一份需求
        rg.add();
        //给出计划
        rg.plan();
        pg.find();
        pg.add();
        pg.plan();
        cg.find();
        cg.add();
        cg.plan();
    }
}