package 命令模式.自我练习;

public abstract class AbsCommand {
    protected final MyGroup myGroup;

    /**
     * 初始化项目组,通过构造函数约定每个具体命令都必须指定接收者，
     * 当然根据开发场景要求也可以有多个接收者，那就需要用集合类型
     *
     * @param myGroup
     */
    public AbsCommand(MyGroup myGroup) {
        this.myGroup = myGroup;
    }

    /**
     * 受气包去执行任务
     */
    public abstract void execute();
}
