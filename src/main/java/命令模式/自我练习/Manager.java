package 命令模式.自我练习;

/**
 * 项目经理
 */
public class Manager {
    private AbsCommand absCommand;

    /**
     * 受气包去接受命令
     *
     * @param absCommand
     */
    public void setAbsCommand(AbsCommand absCommand) {
        this.absCommand = absCommand;
    }

    /**
     * 受气包去执行命令
     */
    public void action() {
        absCommand.execute();
    }
}
