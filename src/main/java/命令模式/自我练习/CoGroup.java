package 命令模式.自我练习;

public class CoGroup extends MyGroup {
    @Override
    public void find() {
        System.out.println("找到代码组");
    }

    @Override
    public void add() {
        System.out.println("代码开发加一点需求");
    }

    @Override
    public void plan() {
        System.out.println("代码开发给出计划表");
    }

    @Override
    public void rollback() {
        System.out.println("用户不上了,根据代码日志给我回滚");
    }
}
