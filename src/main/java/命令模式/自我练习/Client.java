package 命令模式.自我练习;

public class Client {
    public static void main(String[] args) {
        //生成一个受气包
        Manager manager = new Manager();
        //指定人接受命令
        ReqGroup reqGroup = new ReqGroup();
        AbsCommand addCommand = new AddCommand(reqGroup);
        manager.setAbsCommand(addCommand);
        manager.action();
    }
}
