package 命令模式.自我练习;

/**
 * 增加需求的命令
 */
public class AddCommand extends AbsCommand {
    /**
     * 初始化项目组,通过构造函数约定每个具体命令都必须指定接收者，
     * 当然根据开发场景要求也可以有多个接收者，那就需要用集合类型
     *
     * @param myGroup
     */
    public AddCommand(MyGroup myGroup) {
        super(myGroup);
    }

    public AddCommand() {
        super(new ReqGroup());
    }

    @Override
    public void execute() {
        myGroup.find();
        myGroup.add();
        myGroup.plan();

    }
}
