package 命令模式.自我练习;

public abstract class MyGroup {
    public abstract void find();

    public abstract void add();

    public abstract void plan();

    public abstract void rollback();
}
