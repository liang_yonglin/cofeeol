package 命令模式.自我练习;

/**
 * 需求组
 */
public class ReqGroup extends MyGroup {
    @Override
    public void find() {
        System.out.println("找到需求组");
    }

    @Override
    public void add() {
        System.out.println("需求组添加一个需求");
    }

    @Override
    public void plan() {
        System.out.println("需求组给出计划表");
    }

    @Override
    public void rollback() {
        System.out.println("用户不上了,根据需求日志给我回滚");
    }
}
