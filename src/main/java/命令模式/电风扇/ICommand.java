package 命令模式.电风扇;

/**
 * 命令接口
 *
 * @author duguq
 */
public interface ICommand {
    void execute();

    void undo();

}