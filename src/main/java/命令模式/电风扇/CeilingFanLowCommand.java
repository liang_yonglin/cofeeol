package 命令模式.电风扇;

/**
 * 低速运行类
 */
public class CeilingFanLowCommand implements ICommand {
    CeilingFan ceilingFan; // 类中不用 new 方法创建类，降低耦合

    int preSpeed; // 记录执行按键前的状态,便于回测

    public CeilingFanLowCommand(CeilingFan cf) {
        ceilingFan = cf;

    }

    @Override
    public void execute() {
        preSpeed = ceilingFan.getSpeed();
        ceilingFan.Low();

    }

    @Override
    public void undo() {
        switch (preSpeed) {
            case CeilingFan.HIGH:
                ceilingFan.High();
                break;
            case CeilingFan.LOW:
                ceilingFan.Low();
                break;
            default:
                ceilingFan.Off();
                break;
        }

    }

}