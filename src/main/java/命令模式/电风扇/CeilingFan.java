package 命令模式.电风扇;

/**
 * 假定有一个风扇，当前有四个按钮，分别是 高速模式 , 低速模式 , 撤销 ，恢复。
 *
 * @author duguq
 */
public class CeilingFan {
    public static final int HIGH = 2;

    public static final int LOW = 1;

    public static final int OFF = 0;

    int speed;

    public CeilingFan() {
        speed = OFF;
        System.out.println("风扇初始化了");
    }

    public void High() {
        speed = HIGH;
        System.out.println("风扇进入高速状态");
    }

    public void Low() {
        speed = LOW;
        System.out.println("风扇进入低速状态");
    }

    public void Off() {
        speed = OFF;
        System.out.println("风扇关闭了");
    }

    public int getSpeed() {
        return speed;
    }

}
