package 命令模式.电风扇;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * 需要被一个类来调用控制它，这个控制类不仅仅可以控制风扇，同时可以控制电灯，冰箱等等
 *
 * @author duguq
 */
public class Control {
    List<ICommand> onCommands;

    Stack<ICommand> undoCommands;

    Stack<ICommand> redoCommands; // 记录前一个命令, 便于 undo

    public Control() {
        onCommands = new ArrayList<>();

        undoCommands = new Stack<>();

        redoCommands = new Stack<>();

    }

    public void SetCommand(int slot, ICommand onCmd) {
        onCommands.add(slot, onCmd);

    }

    public void OnButtonWasPressed(int slot) {
        if (onCommands.get(slot) != null) {
            onCommands.get(slot).execute();

            undoCommands.push(onCommands.get(slot));

        }

    }

    public void UndoButtonWasPressed() // 撤销,此处用 stack 后进先出的特性

    {
        if (undoCommands.size() > 0) {
            ICommand cmd = undoCommands.pop();

            redoCommands.push(cmd);

            cmd.undo();

        }

    }

    public void RedoButtonWasPressed() {
        if (redoCommands.size() > 0) {
            ICommand cmd = redoCommands.pop();

            undoCommands.push(cmd);

            cmd.execute();

        }

    }

}
