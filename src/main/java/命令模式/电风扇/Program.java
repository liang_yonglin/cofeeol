package 命令模式.电风扇;

/**
 * 调用类
 *
 * @author duguq
 */
public class Program {
    public static void main(String[] args) {
        CeilingFan cf = new CeilingFan();//创建一把风扇
        CeilingFanHighCommand cfh = new CeilingFanHighCommand(cf);//风扇的快速挡位
        CeilingFanLowCommand cfl = new CeilingFanLowCommand(cf);//风扇的低速挡位
        Control ctr = new Control();//控制类
// 假设 button0, button1 分别为高速低速
        ctr.SetCommand(0, cfh);//高速
        ctr.SetCommand(1, cfl);//低速
        ctr.OnButtonWasPressed(0);// 操作了第一下 高速
        ctr.OnButtonWasPressed(1);// 操作了第二下 低速
        ctr.UndoButtonWasPressed();// 撤回了第二下的状态
        ctr.RedoButtonWasPressed();
    }

}
