package 命令模式.电风扇;

/**
 * 关闭类
 *
 * @author duguq
 */
public class CeilingFanOffCommand implements ICommand {
    CeilingFan ceilingFan;
    int preSpeed;

    public CeilingFanOffCommand(CeilingFan cf) {
        ceilingFan = cf;

    }

    @Override
    public void execute() {
        preSpeed = ceilingFan.getSpeed();

        ceilingFan.Off();

    }

    @Override
    public void undo() {
        switch (preSpeed) {
            case CeilingFan.HIGH:
                ceilingFan.High();
                break;

            case CeilingFan.LOW:
                ceilingFan.Low();
                break;
            default:
                ceilingFan.Off();
                break;
        }
    }

}
