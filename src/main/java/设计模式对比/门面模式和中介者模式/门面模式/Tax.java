package 设计模式对比.门面模式和中介者模式.门面模式;

import java.util.Random;

/**
 * 税收
 *
 * @author duguq
 */
public class Tax {
    /**
     * 收取多少税金
     *
     * @return
     */
    public int getTax() {
        //交纳一个随机数量的税金
        return (new Random()).nextInt(300);
    }
}