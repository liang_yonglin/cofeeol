package 设计模式对比.门面模式和中介者模式.门面模式;

/**
 * 总工资计算
 *
 * @author duguq
 */
public class SalaryProvider {
    /**
     * 基本工资
     */
    private BasicSalary basicSalary = new BasicSalary();
    /**
     * 奖金
     */
    private Bonus bonus = new Bonus();
    /**
     * 绩效
     */
    private Performance perf = new Performance();
    /**
     * 税收
     */
    private Tax tax = new Tax();

    /**
     * 获得用户的总收入
     *
     * @return
     */
    public int totalSalary() {
        return basicSalary.getBasicSalary() + bonus.getBonus() + perf.getPerformanceValue() - tax.getTax();
    }
}