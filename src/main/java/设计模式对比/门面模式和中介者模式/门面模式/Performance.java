package 设计模式对比.门面模式和中介者模式.门面模式;

import java.util.Random;

/**
 * 绩效
 *
 * @author duguq
 */
public class Performance {
    /**
     * 基本工资
     */
    private BasicSalary salary = new BasicSalary();

    /**
     * 绩效奖励
     *
     * @return
     */
    public int getPerformanceValue() {
        //随机绩效
        int perf = (new Random()).nextInt(100);
        return salary.getBasicSalary() * perf / 100;
    }
}