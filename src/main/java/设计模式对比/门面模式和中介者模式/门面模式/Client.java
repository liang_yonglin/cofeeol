package 设计模式对比.门面模式和中介者模式.门面模式;

import java.util.Date;

/**
 * 所有的行为都是委托行为，
 * 由具体的子系统实现，
 * 门面只是提供了一个统一访问的基础而已，不做任何的校验、判断、异常等处理。
 * <p>
 * 场景类
 *
 * @author duguq
 */
public class Client {
    public static void main(String[] args) {
        //定义门面
        HRFacade facade = new HRFacade();
        System.out.println("===外系统查询总收入===");
        int salary = facade.querySalary("张三",
                new Date(System.currentTimeMillis()));
        System.out.println("张三 11月 总收入为：" + salary);
        //再查询出勤天数
        System.out.println("\n===外系统查询出勤天数===");
        int workDays = facade.queryWorkDays("李四");
        System.out.println("李四 本月出勤：" + workDays);
    }
}