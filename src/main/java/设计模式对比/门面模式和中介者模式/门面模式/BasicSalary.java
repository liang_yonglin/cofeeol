package 设计模式对比.门面模式和中介者模式.门面模式;

/**
 * 基本工资
 *
 * @author duguq
 */
public class BasicSalary {
    /**
     * 获得一个人的基本工资
     *
     * @return
     */
    public int getBasicSalary() {
        return 2000;
    }
}