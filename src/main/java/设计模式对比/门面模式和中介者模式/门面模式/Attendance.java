package 设计模式对比.门面模式和中介者模式.门面模式;

import java.util.Random;

/**
 * 考勤
 *
 * @author duguq
 */
public class Attendance {
    /**
     * 得到出勤天数
     *
     * @return
     */
    public int getWorkDays() {
        return (new Random()).nextInt(30);
    }
}