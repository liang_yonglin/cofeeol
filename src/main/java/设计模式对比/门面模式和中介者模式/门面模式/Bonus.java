package 设计模式对比.门面模式和中介者模式.门面模式;

/**
 * 奖金
 *
 * @author duguq
 */
public class Bonus {
    /**
     * 考勤
     */
    private Attendance atte = new Attendance();

    /**
     * 奖金
     *
     * @return
     */
    public int getBonus() {
        //获得出勤情况
        int workDays = atte.getWorkDays();
        //奖金计算模型
        int bonus = workDays * 1800 / 30;
        return bonus;
    }
}