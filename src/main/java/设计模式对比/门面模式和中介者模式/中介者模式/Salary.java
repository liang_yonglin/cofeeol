package 设计模式对比.门面模式和中介者模式.中介者模式;

/**
 * 工资
 *
 * @author duguq
 */
public class Salary extends AbsColleague implements ISalary {
    public Salary(AbsMediator _mediator) {
        super(_mediator);
    }

    @Override
    public void decreaseSalary() {
        super.mediator.down(this);
    }

    @Override
    public void increaseSalary() {
        super.mediator.up(this);
    }
}