package 设计模式对比.门面模式和中介者模式.中介者模式;

/**
 * 抽象中介者
 *
 * @author duguq
 */
public abstract class AbsMediator {
    /**
     * 工资
     */
    protected final ISalary salary;
    /**
     * 职位
     */
    protected final IPosition position;
    /**
     * 税收
     */
    protected final ITax tax;

    public AbsMediator() {
        salary = new Salary(this);
        position = new Position(this);
        tax = new Tax(this);
    }

    /**
     * 工资增加
     *
     * @param _salary
     */
    public abstract void up(ISalary _salary);

    /**
     * 职位提升
     *
     * @param _position
     */
    public abstract void up(IPosition _position);

    /**
     * 税收增加
     *
     * @param _tax
     */
    public abstract void up(ITax _tax);

    /**
     * 工资降低
     *
     * @param _salary
     */
    public abstract void down(ISalary _salary);

    /**
     * 职位降低
     *
     * @param _position
     */
    public abstract void down(IPosition _position);

    /**
     * 税收降低
     *
     * @param _tax
     */
    public abstract void down(ITax _tax);
}