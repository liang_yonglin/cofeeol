package 设计模式对比.门面模式和中介者模式.中介者模式;

/**
 * @author duguq
 */
public class Tax extends AbsColleague implements ITax {
    public Tax(AbsMediator _mediator) {
        super(_mediator);
    }

    @Override
    public void drop() {
        super.mediator.down(this);
    }

    @Override
    public void raise() {
        super.mediator.up(this);
    }
}