package 设计模式对比.门面模式和中介者模式.中介者模式;

/**
 * 税收接口
 *
 * @author duguq
 */
public interface ITax {
    /**
     * 税收上升
     */
    public void raise();

    /**
     * 税收下降
     */
    public void drop();
} 