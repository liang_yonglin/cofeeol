package 设计模式对比.门面模式和中介者模式.中介者模式;

/**
 * 职位接口
 *
 * @author duguq
 */
public interface IPosition {
    /**
     * 升职
     */
    void promote();

    /**
     * 降职
     */
    void demote();
}