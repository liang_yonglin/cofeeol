package 设计模式对比.门面模式和中介者模式.中介者模式;

/**
 * 职位
 *
 * @author duguq
 */
public class Position extends AbsColleague implements IPosition {
    public Position(AbsMediator _mediator) {
        super(_mediator);
    }

    @Override
    public void demote() {
        super.mediator.down(this);
    }

    @Override
    public void promote() {
        super.mediator.up(this);
    }
}