package 设计模式对比.门面模式和中介者模式.中介者模式;

/**
 * 抽象同事类
 * <p>
 * 在抽象同事类中定义了每个同事类对中介者都非常了解，如此才能把请求委托给中介者完成。
 */
public abstract class AbsColleague {
    /**
     * 每个同事类都对中介者非常了解
     */
    protected AbsMediator mediator;

    public AbsColleague(AbsMediator _mediator) {
        this.mediator = _mediator;
    }
}