package 设计模式对比.门面模式和中介者模式.中介者模式;

/**
 * 工资接口
 *
 * @author duguq
 */
public interface ISalary {
    /**
     * 加薪
     */
    void increaseSalary();

    /**
     * 降薪
     */
    void decreaseSalary();
}