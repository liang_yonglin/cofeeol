package 设计模式对比.策略模式与桥梁模式.策略模式;

/**
 * 邮件模版
 *
 * @author duguq
 */
public abstract class MailTemplate {
    /**
     * 邮件发件人
     */
    private String from;
    /**
     * 收件人
     */
    private String to;
    /**
     * 邮件标题
     */
    private String subject;
    /**
     * 邮件内容
     */
    private String context;

    /**
     * 通过构造函数传递邮件信息
     *
     * @param _from
     * @param _to
     * @param _subject
     * @param _context
     */
    public MailTemplate(String _from, String _to, String _subject, String _context) {
        this.from = _from;
        this.to = _to;
        this.subject = _subject;
        this.context = _context;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }
}