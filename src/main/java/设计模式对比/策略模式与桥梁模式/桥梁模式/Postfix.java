package 设计模式对比.策略模式与桥梁模式.桥梁模式;

/**
 * Postfix邮件服务器
 *
 * @author duguq
 */
public class Postfix extends MailServer {
    public Postfix(MailTemplate _m) {
        super(_m);
    }

    @Override
    public void sendMail() {
        //增加邮件服务器信息
        String context = "Received: from XXXX (unknown [xxx.xxx.xxx.xxx]) by aaa.aaa.com (Postfix) with ESMTP id 8DBCD172B8\n";
        super.m.add(context);
        super.sendMail();
    }
}