package 设计模式对比.策略模式与桥梁模式.桥梁模式;

/**
 * SendMail邮件服务器
 *
 * @author duguq
 */
public class SendMail extends MailServer {
    /**
     * 传递一封邮件
     *
     * @param _m
     */
    public SendMail(MailTemplate _m) {
        super(_m);
    }

    @Override
    public void sendMail() {
        //增加邮件服务器信息
        super.m.add("Received: (sendmail); 7 Nov 2009 04:14:44 +0100");
        super.sendMail();
    }
}