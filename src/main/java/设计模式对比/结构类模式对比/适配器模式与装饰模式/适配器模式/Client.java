package 设计模式对比.结构类模式对比.适配器模式与装饰模式.适配器模式;

/**
 * 采用适配器模式实现丑小鸭变成白天鹅的过程要从鸭妈妈的角度来分析，
 * 鸭妈妈有5个孩子，它认为这5个孩子都是她的后代，都是鸭类，但是实际上是有一只（也就是丑小鸭）不是真正的鸭类，
 * 她是一只小白天鹅，就像《木兰辞》中说的“雄兔脚扑朔，雌兔眼迷离。双兔傍地走，安能辨我是雄雌？”同样，因为太小，差别太细微，很难分辨，导致鸭妈妈认为她是一只鸭子，
 * 从鸭子的审美观来看，丑小鸭是丑陋的。通过分析，我们要做的就是要设计两个对象：鸭和天鹅，然后鸭妈妈把一只天鹅看成了小鸭子，最终时间到来的时候丑小鸭变成了白天鹅。
 *
 * @author duguq
 */
public class Client {
    public static void main(String[] args) {
        //鸭妈妈有5个孩子，其中4个都是一个模样
        System.out.println("===妈妈有五个孩子，其中四个模样是这样的：===");
        Duck duck = new Duckling();
        duck.cry();  //小鸭子的叫声
        duck.desAppearance(); //小鸭子的外形
        duck.desBehavior(); //小鸭子的其他行为
        System.out.println("\n===一只独特的小鸭子，模样是这样的：===");
        Duck uglyDuckling = new UglyDuckling();  // 实现了呃duck接口,但是实际上他是一个swan的派生类,伪装成了duck
        uglyDuckling.cry(); //丑小鸭的叫声
        uglyDuckling.desAppearance(); //丑小鸭的外形
        uglyDuckling.desBehavior(); //丑小鸭的其他行为
    }
}