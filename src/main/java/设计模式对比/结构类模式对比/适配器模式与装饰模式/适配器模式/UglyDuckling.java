package 设计模式对比.结构类模式对比.适配器模式与装饰模式.适配器模式;


/**
 * 需要的时候（根据故事情节）把一个物种伪装成另外一个物种，实现不同物种的相同处理过程，这就是适配器模式的设计意图
 *
 * @author duguq
 */
public class UglyDuckling extends WhiteSwan implements Duck {
    //丑小鸭的叫声
    @Override
    public void cry() {
        super.cry();
    }

    //丑小鸭的外形
    @Override
    public void desAppearance() {
        super.desAppaearance();
    }

    //丑小鸭的其他行为
    @Override
    public void desBehavior() {
        //丑小鸭不仅会游泳
        System.out.println("会游泳");
        //还会飞行
        fly();
    }
}