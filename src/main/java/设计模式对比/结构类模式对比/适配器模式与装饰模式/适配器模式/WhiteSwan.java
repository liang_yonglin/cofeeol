package 设计模式对比.结构类模式对比.适配器模式与装饰模式.适配器模式;

import 设计模式对比.结构类模式对比.适配器模式与装饰模式.装饰模式.Swan;

public class WhiteSwan implements Swan {
    //白天鹅的叫声
    @Override
    public void cry() {
        System.out.println("叫声是克噜——克噜——克噜");
    }

    //白天鹅的外形
    @Override
    public void desAppaearance() {
        System.out.println("外形是纯白色，惹人喜爱");
    }

    //天鹅是能够飞行的
    @Override
    public void fly() {
        System.out.println("能够飞行");
    }
}