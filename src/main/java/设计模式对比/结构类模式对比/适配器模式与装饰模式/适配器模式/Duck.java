package 设计模式对比.结构类模式对比.适配器模式与装饰模式.适配器模式;

public interface Duck {
    //会叫
    void cry();

    //鸭子的外形
    void desAppearance();

    //描述鸭子的其他行为
    void desBehavior();
}