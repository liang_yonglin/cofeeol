package 设计模式对比.结构类模式对比.适配器模式与装饰模式.装饰模式;

public class StrongBehavior extends Decorator {
    //强化谁
    public StrongBehavior(Swan _swan) {
        super(_swan);
    }

    //会飞行了
    @Override
    public void fly() {
        System.out.println("会飞行了！");
    }
}