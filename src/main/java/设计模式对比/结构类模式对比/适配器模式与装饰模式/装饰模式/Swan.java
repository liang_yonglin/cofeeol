package 设计模式对比.结构类模式对比.适配器模式与装饰模式.装饰模式;

/**
 * @author duguq
 */
public interface Swan {
    //天鹅会飞
    void fly();

    //天鹅会叫
    void cry();

    //天鹅都有漂亮的外表
    void desAppaearance();
}