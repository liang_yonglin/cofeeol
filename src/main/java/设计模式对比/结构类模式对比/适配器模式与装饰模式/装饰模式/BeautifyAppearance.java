package 设计模式对比.结构类模式对比.适配器模式与装饰模式.装饰模式;

public class BeautifyAppearance extends Decorator {
    //要美化谁
    public BeautifyAppearance(Swan _swan) {
        super(_swan);
    }

    //外表美化处理
    @Override
    public void desAppaearance() {
        System.out.println("外表是纯白色的，非常惹人喜爱！");
    }
}