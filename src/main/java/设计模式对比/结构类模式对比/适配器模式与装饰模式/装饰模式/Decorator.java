package 设计模式对比.结构类模式对比.适配器模式与装饰模式.装饰模式;

public class Decorator implements Swan {
    private final Swan swan;

    //修饰的是谁
    public Decorator(Swan _swan) {
        this.swan = _swan;
    }

    @Override
    public void cry() {
        swan.cry();
    }

    @Override
    public void desAppaearance() {
        swan.desAppaearance();
    }

    @Override
    public void fly() {
        swan.fly();
    }
}