package 设计模式对比.结构类模式对比.装饰模式;

public interface IRunner {
    //运动员的主要工作就是跑步
    void run();
}