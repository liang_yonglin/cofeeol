package 设计模式对比.结构类模式对比.装饰模式;


/**
 * @author duguq
 */
public class RunnerWithJet implements IRunner {
    private final IRunner runner;

    public RunnerWithJet(IRunner _runner) {
        this.runner = _runner;
    }

    @Override
    public void run() {
        System.out.println("加快运动员的速度：为运动员增加喷气装置");
        runner.run();
    }
}