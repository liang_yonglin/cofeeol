package 设计模式对比.结构类模式对比.代理模式;

import java.util.Random;

public class RunnerAgent implements IRunner {
    private final IRunner runner;

    public RunnerAgent(IRunner _runner) {
        runner = _runner;
    }

    //代理人是不会跑的
    @Override
    public void run() {
        Random rand = new Random();
        if (rand.nextBoolean()) {
            System.out.println("代理人同意安排运动员跑步");
            runner.run();
        } else {
            System.out.println("代理人心情不好，不安排运动员跑步");
        }
    }
}