package 设计模式对比.结构类模式对比.代理模式;

public class Client {
    public static void main(String[] args) {
        //定义一个短跑运动员
        IRunner liu = new Runner();
        //定义liu的代理人
        IRunner agent = new RunnerAgent(liu); // 不直接用liu,而是用liu的代理,各种增强可以在liu的代理中做到,不需要连累到liu

        // liu.run();
        // 要求运动员跑步
        System.out.println("====客人找到运动员的代理要求其去跑步===");
        agent.run();
    }
}