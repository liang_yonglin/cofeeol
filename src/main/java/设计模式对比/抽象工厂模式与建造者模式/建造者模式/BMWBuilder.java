package 设计模式对比.抽象工厂模式与建造者模式.建造者模式;

public class BMWBuilder extends CarBuilder {
    @Override
    public String buildEngine() {
        return super.getBlueprint().getEngine();
    }

    @Override
    public String buildWheel() {
        return super.getBlueprint().getWheel();
    }
}