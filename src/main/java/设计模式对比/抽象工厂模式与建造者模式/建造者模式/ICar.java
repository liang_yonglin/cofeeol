package 设计模式对比.抽象工厂模式与建造者模式.建造者模式;

public interface ICar {
    //汽车车轮
    String getWheel();

    //汽车引擎
    String getEngine();
}