package 设计模式对比.抽象工厂模式与建造者模式.建造者模式;

public class Car implements ICar {
    //汽车引擎
    private final String engine;
    //汽车车轮
    private final String wheel;

    //一次性传递汽车需要的信息
    public Car(String _engine, String _wheel) {
        this.engine = _engine;
        this.wheel = _wheel;
    }

    @Override
    public String getEngine() {
        return engine;
    }

    @Override
    public String getWheel() {
        return wheel;
    }

    @Override
    public String toString() {
        return "车的轮子是：" + wheel + "\n车的引擎是：" + engine;
    }
}