package 设计模式对比.抽象工厂模式与建造者模式.建造者模式;

public class Director {
    //声明对建造者的引用
    private final CarBuilder benzBuilder = new BenzBuilder();
    private final CarBuilder bmwBuilder = new BMWBuilder();
    private final CarBuilder cpxBuilder = new ComplexBuilder();

    //生产奔驰SUV
    public ICar createBenzSuv() {
        //制造出汽车
        return createCar(benzBuilder, "benz的引擎", "benz的轮胎");
    }

    //生产出一辆宝马商务车
    public ICar createBMWVan() {
        return createCar(bmwBuilder, "BMW的引擎", "BMW的轮胎");
    }

    //生产出一个混合车型
    public ICar createComplexCar() {
        return createCar(cpxBuilder, "BMW的引擎", "benz的轮胎");
    }

    //生产车辆
    private ICar createCar(CarBuilder _carBuilder, String engine, String wheel) {
        //导演怀揣蓝图
        Blueprint bp = new Blueprint();
        bp.setEngine(engine);
        bp.setWheel(wheel);
        System.out.println("获得生产蓝图");
        _carBuilder.receiveBlueprint(bp);
        return _carBuilder.buildCar();
    }
}