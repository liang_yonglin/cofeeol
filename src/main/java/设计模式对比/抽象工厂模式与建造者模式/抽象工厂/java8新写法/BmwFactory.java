package 设计模式对比.抽象工厂模式与建造者模式.抽象工厂.java8新写法;

import java.util.function.Supplier;

/**
 * 宝马的工厂方法
 *
 * @author yonglianglin
 */
public class BmwFactory extends CarFactory {

    @Override
    public Supplier<ICar> getSuv() {
        return BMWSuv::new;
    }

    @Override
    public void setVan(Supplier<ICar> van) {
        this.van = BMWVan::new;
    }
}