package 设计模式对比.抽象工厂模式与建造者模式.抽象工厂.java8新写法;

public class BMWVan extends AbsBMW {
    private final static String SEVENT_SEARIES = "7系列车型商务车";

    @Override
    public String getModel() {
        return SEVENT_SEARIES;
    }
}