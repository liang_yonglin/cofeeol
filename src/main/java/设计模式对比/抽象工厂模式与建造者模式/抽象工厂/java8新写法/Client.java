package 设计模式对比.抽象工厂模式与建造者模式.抽象工厂.java8新写法;

/**
 * @author duguq
 */
public class Client {
    public static void main(String[] args) {
        //要求生产一辆奔驰SUV
        System.out.println("===要求生产一辆奔驰SUV===");
        //首先找到生产奔驰车的工厂
        System.out.println("A、找到奔驰车工厂");
        CarFactory carFactory = new BenzFactory();
        //开始生产奔驰SUV
        System.out.println("B、开始生产奔驰SUV");
        ICar benzSuv = carFactory.suv.get();
        //生产完毕，展示一下车辆信息
        System.out.println("C、生产出的汽车如下：");
        System.out.println("汽车品牌：" + benzSuv.getBand());
        System.out.println("汽车型号：" + benzSuv.getModel());

        // fixme 很明显创造型的工厂，不适合用get，set方法去创造，直接用构造方法赋值会方便许多，也比较符合思考的逻辑
        CarFactory bmwFactory = new BmwFactory();
        //开始生产奔驰SUV
        System.out.println("D、开始生产奔驰SUV");
        bmwFactory.setVan(BMWVan::new);
        ICar bmw = bmwFactory.getVan().get();
        // ICar bmw = bmwFactory.suv.get();
        //生产完毕，展示一下车辆信息
        System.out.println("E、生产出的汽车如下：");
        System.out.println("汽车品牌：" + bmw.getBand());
        System.out.println("汽车型号：" + bmw.getModel());
    }
}