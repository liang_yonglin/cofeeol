package 设计模式对比.抽象工厂模式与建造者模式.抽象工厂.java8新写法;

public abstract class AbsBMW implements ICar {
    private final static String BMW_BAND = "宝马汽车";

    //宝马车
    @Override
    public String getBand() {
        return BMW_BAND;
    }

    //型号由具体的实现类实现
    @Override
    public abstract String getModel();
}