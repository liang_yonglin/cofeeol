package 设计模式对比.抽象工厂模式与建造者模式.抽象工厂.java8新写法;

public class BenzSuv extends AbsBenz {
    private final static String G_SERIES = "G系列SUV";

    @Override
    public String getModel() {
        return G_SERIES;
    }
}