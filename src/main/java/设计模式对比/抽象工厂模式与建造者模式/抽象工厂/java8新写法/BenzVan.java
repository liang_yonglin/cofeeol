package 设计模式对比.抽象工厂模式与建造者模式.抽象工厂.java8新写法;

public class BenzVan extends AbsBenz {
    private final static String R_SERIES = "R系列商务车";

    @Override
    public String getModel() {
        return R_SERIES;
    }
}