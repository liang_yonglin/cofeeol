package 设计模式对比.抽象工厂模式与建造者模式.抽象工厂.java8新写法;

/**
 * 具体的工厂
 * 奔驰工厂
 *
 * @author yonglianglin
 */
public class BenzFactory extends CarFactory {

    /**
     * 执行构造方法的时候把两种车型生产出来
     */
    public BenzFactory() {
        suv = BenzSuv::new;
        van = BenzVan::new;
    }


}