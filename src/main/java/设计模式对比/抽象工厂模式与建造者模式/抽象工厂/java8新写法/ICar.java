package 设计模式对比.抽象工厂模式与建造者模式.抽象工厂.java8新写法;

/**
 * 汽车接口
 *
 * @author yonglianglin
 */
public interface ICar {
    /**
     * 汽车的生产商，也就是牌子
     *
     * @return
     */

    String getBand();

    /**
     * 汽车的型号
     *
     * @return
     */
    String getModel();
}