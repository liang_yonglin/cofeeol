package 设计模式对比.抽象工厂模式与建造者模式.抽象工厂.java8新写法;

import java.util.function.Supplier;

/**
 * 抽象工厂
 * 通过Supplier来提供创造方法,以为supplier已经包含了创建的意思了
 *
 * @author yonglianglin
 */
public class CarFactory {

    /**
     * 生产SUV
     */
    protected Supplier<ICar> suv;
    /**
     * 生产商务车
     */
    protected Supplier<ICar> van;


    public Supplier<ICar> getSuv() {
        return suv;
    }

    public void setSuv(Supplier<ICar> suv) {
        this.suv = suv;
    }

    public Supplier<ICar> getVan() {
        return van;
    }

    public void setVan(Supplier<ICar> van) {
        this.van = van;
    }
}