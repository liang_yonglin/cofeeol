package 设计模式对比.抽象工厂模式与建造者模式.抽象工厂;

public interface CarFactory {
    //生产SUV
    ICar createSuv();

    //生产商务车
    ICar createVan();
}