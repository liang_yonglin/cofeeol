package 设计模式对比.抽象工厂模式与建造者模式.抽象工厂;

public abstract class AbsBenz implements ICar {
    private final static String BENZ_BAND = "奔驰汽车";

    public String getBand() {
        return BENZ_BAND;
    }

    //具体型号由实现类完成
    public abstract String getModel();
}