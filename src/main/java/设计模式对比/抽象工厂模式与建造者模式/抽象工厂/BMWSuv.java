package 设计模式对比.抽象工厂模式与建造者模式.抽象工厂;

public class BMWSuv extends AbsBMW {
    private final static String X_SEARIES = "X系列车型SUV";

    public String getModel() {
        return X_SEARIES;
    }
}