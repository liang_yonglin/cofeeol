package 设计模式对比.抽象工厂模式与建造者模式.抽象工厂;

public class BenzFactory implements CarFactory {
    //生产SUV
    public ICar createSuv() {
        return new BenzSuv();
    }

    //生产商务车
    public ICar createVan() {
        return new BenzVan();
    }
}