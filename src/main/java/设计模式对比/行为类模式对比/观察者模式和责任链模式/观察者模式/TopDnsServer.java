package 设计模式对比.行为类模式对比.观察者模式和责任链模式.观察者模式;

/**
 * 全球顶级DNS服务器
 *
 * @author duguq
 */
public class TopDnsServer extends DnsServer {
    @Override
    protected void sign(Recorder recorder) {
        recorder.setOwner("全球顶级DNS服务器");
    }

    @Override
    protected boolean isLocal(Recorder recorder) {
        //所有的域名最终的解析地点
        return true;
    }
}