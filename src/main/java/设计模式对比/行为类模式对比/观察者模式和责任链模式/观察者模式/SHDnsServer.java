package 设计模式对比.行为类模式对比.观察者模式和责任链模式.观察者模式;

/**
 * 上海DNS服务器
 *
 * @author duguq
 */
public class SHDnsServer extends DnsServer {
    @Override
    protected void sign(Recorder recorder) {
        recorder.setOwner("上海DNS服务器");
    }

    //定义上海的DNS服务器能处理的级别
    @Override
    protected boolean isLocal(Recorder recorder) {
        return recorder.getDomain().endsWith(".sh.cn");
    }
}