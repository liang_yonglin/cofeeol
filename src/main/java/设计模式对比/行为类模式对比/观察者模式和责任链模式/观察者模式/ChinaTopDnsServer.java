package 设计模式对比.行为类模式对比.观察者模式和责任链模式.观察者模式;

/**
 * 中国顶级DNS服务器
 *
 * @author duguq
 */
public class ChinaTopDnsServer extends DnsServer {
    @Override
    protected void sign(Recorder recorder) {
        recorder.setOwner("中国顶级DNS服务器");
    }

    @Override
    protected boolean isLocal(Recorder recorder) {
        return recorder.getDomain().endsWith(".cn");
    }
}