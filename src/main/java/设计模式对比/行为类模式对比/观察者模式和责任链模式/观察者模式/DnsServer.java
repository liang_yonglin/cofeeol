package 设计模式对比.行为类模式对比.观察者模式和责任链模式.观察者模式;

import java.util.Observable;
import java.util.Observer;
import java.util.Random;

/**
 * 抽象DNS服务器
 *
 * @author duguq
 */
public abstract class DnsServer extends Observable implements Observer {
    /**
     * 处理请求，也就是接收到事件后的处理
     *
     * @param arg0
     * @param arg1
     */
    @Override
    public void update(Observable arg0, Object arg1) {
        Recorder recorder = (Recorder) arg1;
        //如果本机能解析
        if (isLocal(recorder)) {
            recorder.setIp(genIpAddress());
        } else {//本机不能解析，则提交到上级DNS
            responseFromUpperServer(recorder);
        }
        //签名
        sign(recorder);
    }

    /**
     * 作为被观察者，允许增加观察者，这里上级DNS一般只有一个
     *
     * @param dnsServer
     */
    public void setUpperServer(DnsServer dnsServer) {
        //先清空，然后再增加
        super.deleteObservers();
        super.addObserver(dnsServer);
    }

    /**
     * 向父DNS请求解析，也就是通知观察者
     *
     * @param recorder
     */
    private void responseFromUpperServer(Recorder recorder) {
        super.setChanged();
        super.notifyObservers(recorder);
    }

    /**
     * 每个DNS服务器签上自己的名字
     *
     * @param recorder
     */
    protected abstract void sign(Recorder recorder);

    /**
     * 每个DNS服务器都必须定义自己的处理级别
     *
     * @param recorder
     * @return
     */
    protected abstract boolean isLocal(Recorder recorder);

    /**
     * 随机产生一个IP地址，工具类
     *
     * @return
     */
    private String genIpAddress() {
        Random rand = new Random();
        String address = rand.nextInt(255) + "." + rand.nextInt(255) + "." + rand.nextInt(255) + "." + rand.nextInt(255);
        return address;
    }
}