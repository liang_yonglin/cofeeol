package 设计模式对比.行为类模式对比.观察者模式和责任链模式.责任链模式;

/**
 * 中国顶级DNS服务器
 *
 * @author duguq
 */
public class ChinaTopDnsServer extends DnsServer {
    @Override
    protected Recorder echo(String domain) {
        Recorder recorder = super.echo(domain);
        recorder.setOwner("中国顶级DNS服务器");
        return recorder;
    }

    @Override
    protected boolean isLocal(String domain) {
        return domain.endsWith(".cn");
    }
}