package 设计模式对比.行为类模式对比.策略模式和状态模式.策略模式;

/**
 * 环境角色
 *
 * @author duguq
 */
public class Context {
    private WorkAlgorithm workMethod;

    public WorkAlgorithm getWork() {
        return workMethod;
    }

    public void setWork(WorkAlgorithm work) {
        this.workMethod = work;
    }

    /**
     * 每个算法都有必须具有的功能
     */
    public void work() {
        workMethod.work();
    }
}