package 设计模式对比.行为类模式对比.策略模式和状态模式.策略模式;

/**
 * 孩童工作算法
 *
 * @author duguq
 */
public class ChildWork extends WorkAlgorithm {
    @Override
    public void work() {
        System.out.println("儿童的工作是玩耍！");
    }
}