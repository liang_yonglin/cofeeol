package 设计模式对比.行为类模式对比.策略模式和状态模式.策略模式;

/**
 * 抽象工作算法
 *
 * @author duguq
 */
public abstract class WorkAlgorithm {
    /**
     * 每个年龄段都必须完成的工作算法
     */
    public abstract void work();
}