package 设计模式对比.行为类模式对比.策略模式和状态模式.策略模式;

/**
 * 成人工作算法
 *
 * @author duguq
 */
public class AdultWork extends WorkAlgorithm {
    //成年人的工作
    @Override
    public void work() {
        System.out.println("成年人的工作就是先养活自己，然后为社会做贡献！");
    }
}