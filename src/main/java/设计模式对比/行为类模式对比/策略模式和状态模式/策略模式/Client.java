package 设计模式对比.行为类模式对比.策略模式和状态模式.策略模式;

/**
 * 场景类
 * 通过采用策略模式我们实现了“工作”这个策略的三种不同算法，算法可以自由切换，到底用哪个算法由调用者（高层模块）决定。
 * 策略模式的使用重点是算法的自由切换——老的算法退休，新的算法上台，对模块的整体功能没有非常大的改变，非常灵活。
 * 而如果想要增加一个新的算法，比如未出生婴儿的工作，只要继承WorkAlgorithm就可以了。
 */
public class Client {
    public static void main(String[] args) {
        //定义一个环境角色
        Context context = new Context();
        System.out.println("====儿童的主要工作=====");
        context.setWork(new ChildWork());
        context.work();
        System.out.println("\n====成年人的主要工作=====");
        context.setWork(new AdultWork());
        context.work();
        System.out.println("\n====老年人的主要工作=====");
        context.setWork(new OldWork());
        context.work();
    }
}