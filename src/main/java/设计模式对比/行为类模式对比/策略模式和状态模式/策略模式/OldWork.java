package 设计模式对比.行为类模式对比.策略模式和状态模式.策略模式;

/**
 * 老人工作算法
 *
 * @author duguq
 */
public class OldWork extends WorkAlgorithm {
    @Override
    public void work() {
        System.out.println("老年人的工作就是享受天伦之乐！");
    }
}