package 设计模式对比.行为类模式对比.策略模式和状态模式.状态模式;

/**
 * 环境角色
 *
 * @author duguq
 */
public class Human {
    //定义人类都具备哪些状态
    public static final HumanState CHIILD_STATE = new ChildState();
    public static final HumanState ADULT_STATE = new AdultState();
    public static final HumanState OLD_STATE = new OldState();
    //定义一个人的状态
    private HumanState state;

    //设置一个状态
    public void setState(HumanState _state) {
        this.state = _state;
        this.state.setHuman(this);
    }

    //人类的工作
    public void work() {
        this.state.work();
    }
}