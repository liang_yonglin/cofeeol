package 设计模式对比.行为类模式对比.策略模式和状态模式.状态模式;

/**
 * 老年人状态
 *
 * @author duguq
 */
public class OldState extends HumanState {
    //老年人的工作就是享受天伦之乐
    @Override
    public void work() {
        System.out.println("老年人的工作就是享受天伦之乐！");
    }
}