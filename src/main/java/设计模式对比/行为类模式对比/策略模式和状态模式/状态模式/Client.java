package 设计模式对比.行为类模式对比.策略模式和状态模式.状态模式;

/**
 * 场景类
 * 运行结果与策略模式相同，但是两者的分析角度是大相径庭的。
 * 策略模式的实现是通过分析每个人的工作方式的不同而得出三个不同的算法逻辑，
 * 状态模式则是从人的生长规律来分析，每个状态对应了不同的行为，状态改变后行为也随之改变。
 * 对于相同的业务需求，有很多种实现方法，问题的重点是业务关注的是什么，是人的生长规律还是工作逻辑？
 * 找准了业务的焦点，才能选择一个好的设计模式。
 *
 * @author duguq
 */
public class Client {
    public static void main(String[] args) {
        //定义一个普通的人
        Human human = new Human();
        //设置一个人的初始状态
        human.setState(new ChildState());
        System.out.println("====儿童的主要工作=====");
        human.work();
        System.out.println("\n====成年人的主要工作=====");
        human.work();
        System.out.println("\n====老年人的主要工作=====");
        human.work();
    }
}