package 设计模式对比.行为类模式对比.策略模式和状态模式.状态模式;

/**
 * 孩童状态
 *
 * @author duguq
 */
public class ChildState extends HumanState {
    //儿童的工作就是玩耍
    @Override
    public void work() {
        System.out.println("儿童的工作是玩耍！");
        super.human.setState(Human.ADULT_STATE);
    }
}