package 设计模式对比.行为类模式对比.策略模式和状态模式.状态模式;

/**
 * 人的抽象状态
 *
 * @author duguq
 */
public abstract class HumanState {
    //指向一个具体的人
    protected Human human;

    //设置一个具体的人
    public void setHuman(Human _human) {
        this.human = _human;
    }

    //不管人是什么状态都要工作
    public abstract void work();
}