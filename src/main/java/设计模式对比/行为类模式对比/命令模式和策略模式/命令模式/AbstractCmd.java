package 设计模式对比.行为类模式对比.命令模式和策略模式.命令模式;

/**
 * 抽象压缩命令
 *
 * @author duguq
 */
public abstract class AbstractCmd {
    //对接收者的引用
    protected IReceiver zip = new ZipReceiver();
    protected IReceiver gzip = new GzipReceiver();

    //抽象方法，命令的具体单元
    public abstract boolean execute(String source, String to);
}