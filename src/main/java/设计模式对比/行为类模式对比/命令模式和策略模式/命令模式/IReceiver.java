package 设计模式对比.行为类模式对比.命令模式和策略模式.命令模式;

/**
 * 抽象接收者
 * 其实现类是具体动作的执行者
 *
 * @author duguq
 */
public interface IReceiver {
    /**
     * 压缩
     *
     * @param source
     * @param to
     * @return
     */
    boolean compress(String source, String to);

    /**
     * 解压缩
     *
     * @param source
     * @param to
     * @return
     */
    boolean uncompress(String source, String to);


}