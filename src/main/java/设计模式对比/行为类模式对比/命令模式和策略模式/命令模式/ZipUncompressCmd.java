package 设计模式对比.行为类模式对比.命令模式和策略模式.命令模式;

/**
 * zip解压缩命令
 *
 * @author duguq
 */
public class ZipUncompressCmd extends AbstractCmd {
    @Override
    public boolean execute(String source, String to) {
        return super.zip.uncompress(source, to);
    }
}