package 设计模式对比.行为类模式对比.命令模式和策略模式.命令模式;

/**
 * zip压缩命令
 *
 * @author duguq
 */
public class ZipCompressCmd extends AbstractCmd {
    @Override
    public boolean execute(String source, String to) {
        return super.zip.compress(source, to);
    }
}