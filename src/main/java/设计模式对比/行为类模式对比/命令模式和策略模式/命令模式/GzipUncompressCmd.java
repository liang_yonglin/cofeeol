package 设计模式对比.行为类模式对比.命令模式和策略模式.命令模式;

/**
 * gzip解压缩命令
 */
public class GzipUncompressCmd extends AbstractCmd {
    @Override
    public boolean execute(String source, String to) {
        return super.gzip.uncompress(source, to);
    }
}