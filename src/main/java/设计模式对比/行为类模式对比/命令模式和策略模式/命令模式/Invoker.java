package 设计模式对比.行为类模式对比.命令模式和策略模式.命令模式;

public class Invoker {
    //抽象命令的引用
    private final AbstractCmd cmd;

    public Invoker(AbstractCmd _cmd) {
        this.cmd = _cmd;
    }

    //执行命令
    public boolean execute(String source, String to) {
        return cmd.execute(source, to);
    }
}