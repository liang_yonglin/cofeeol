package 设计模式对比.行为类模式对比.命令模式和策略模式.策略模式;

/**
 * 算法
 *
 * @author duguq
 */
public interface Algorithm {
    /**
     * 压缩算法
     *
     * @param source
     * @param to
     * @return
     */
    boolean compress(String source, String to);

    /**
     * 解压缩算法
     *
     * @param source
     * @param to
     * @return
     */
    boolean uncompress(String source, String to);
}