package 设计模式对比.行为类模式对比.命令模式和策略模式.策略模式;

/**
 * @author duguq
 */
public class Zip implements Algorithm {
    //zip格式的压缩算法
    @Override
    public boolean compress(String source, String to) {
        System.out.println(source + " --> " + to + " ZIP压缩成功!");
        return true;
    }

    //zip格式的解压缩算法
    @Override
    public boolean uncompress(String source, String to) {
        System.out.println(source + " --> " + to + " ZIP解压缩成功!");
        return true;
    }
}