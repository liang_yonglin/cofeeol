package 设计模式对比.行为类模式对比.命令模式和策略模式.策略模式;

/**
 * 环境角色
 *
 * @author duguq
 */
public class Context {
    //指向抽象算法
    private final Algorithm al;

    //构造函数传递具体的算法
    public Context(Algorithm _al) {
        this.al = _al;
    }

    //执行压缩算法
    public boolean compress(String source, String to) {
        return al.compress(source, to);
    }

    //执行解压缩算法
    public boolean uncompress(String source, String to) {
        return al.uncompress(source, to);
    }
}