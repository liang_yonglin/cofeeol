package 设计模式对比.工厂方法模式与建造者模式.工厂模式;

public class Client {
    //模拟生产超人
    public static void main(String[] args) {
        //生产一个成年超人
        ISuperMan adultSuperMan = SuperManFactory.createSuperMan("adult");
        //展示一下超人的技能
        adultSuperMan.specialTalent();
    }
}