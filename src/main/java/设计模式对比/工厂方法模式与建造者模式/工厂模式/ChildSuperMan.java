package 设计模式对比.工厂方法模式与建造者模式.工厂模式;

public class ChildSuperMan implements ISuperMan {
    //超能先生的三个孩子
    @Override
    public void specialTalent() {
        System.out.println("小超人的能力是刀枪不入、快速运动");
    }
}