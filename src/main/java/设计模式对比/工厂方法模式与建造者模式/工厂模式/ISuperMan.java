package 设计模式对比.工厂方法模式与建造者模式.工厂模式;

public interface ISuperMan {
    //每个超人都有特殊技能
    void specialTalent();
}