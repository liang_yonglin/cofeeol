package 设计模式对比.工厂方法模式与建造者模式.建造者模式;

public class Client {
    public static void main(String[] args) {
        //建造一个成年超人
        SuperMan adultSuperMan = Director.getAdultSuperMan();
        //展示一下超人的信息
        System.out.println(adultSuperMan.getSpecialTalent());
    }
}