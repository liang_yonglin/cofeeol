package 状态模式.基础;

/**
 * 电梯接口
 *
 * @author duguq
 */
public interface ILift {
    /**
     * 电梯门开启动作
     */
    void open();

    /**
     * 电梯门关闭
     */
    void close();

    /**
     * 电梯要能上下
     */
    void run();

    /**
     * 电梯还要能停下来
     */
    void stop();
}