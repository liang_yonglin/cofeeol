package 状态模式.拓展;

import 状态模式.拓展.状态.ClosingState;
import 状态模式.拓展.状态.OpenningState;
import 状态模式.拓展.状态.RunningState;
import 状态模式.拓展.状态.StoppingState;

/**
 * Context是一个环境角色，它的作用是串联各个状态的过渡
 *
 * @author duguq
 */
public class Context {
    //定义出所有的电梯状态
    /**
     * 开门状态
     */
    public final static OpenningState openningState = new OpenningState();
    /**
     * 关门状态
     */
    public final static ClosingState closingState = new ClosingState();
    /**
     * 运行状态
     */
    public final static RunningState runningState = new RunningState();
    /**
     * 停止状态
     */
    public final static StoppingState stoppingState = new StoppingState();
    //定义一个当前电梯状态
    private LiftState liftState;

    public LiftState getLiftState() {
        return liftState;
    }

    public void setLiftState(LiftState liftState) {
        this.liftState = liftState;
        //把当前的环境通知到各个实现类中
        this.liftState.setContext(this);
    }

    public void open() {
        this.liftState.open();
    }

    public void close() {
        this.liftState.close();
    }

    public void run() {
        this.liftState.run();
    }

    public void stop() {
        this.liftState.stop();
    }
}