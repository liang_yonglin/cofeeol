package 状态模式.拓展.状态;

import 状态模式.拓展.Context;
import 状态模式.拓展.LiftState;

/**
 * 敞门状态
 *
 * @author duguq
 */
public class OpenningState extends LiftState {
    @Override
    public void close() {
        //状态修改
        super.context.setLiftState(Context.closingState);
        //动作委托为CloseState来执行
        super.context.getLiftState().close();
    }

    @Override
    public void open() {
        System.out.println("电梯门开启...");
    }

    @Override
    public void run() {
        //do nothing;
    }

    @Override
    public void stop() {
        //do nothing;
    }
}