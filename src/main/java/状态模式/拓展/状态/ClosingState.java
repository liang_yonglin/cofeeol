package 状态模式.拓展.状态;

import 状态模式.拓展.Context;
import 状态模式.拓展.LiftState;

/**
 * 关闭状态
 *
 * @author duguq
 */
public class ClosingState extends LiftState {
    @Override
    public void close() {
        System.out.println("电梯门关闭...");
    }

    //电梯门关了再打开
    @Override
    public void open() {
        // 让状态顺利过渡
        context.setLiftState(Context.openningState);  //置为敞门状态
        // 获取电梯状态
        LiftState liftState = context.getLiftState();
        // 电梯门开启
        liftState.open();
    }

    //电梯门关了就运行，这是再正常不过了
    @Override
    public void run() {
        super.context.setLiftState(Context.runningState); //设置为运行状态
        super.context.getLiftState().run();
    }

    //电梯门关着，我就不按楼层
    @Override
    public void stop() {
        super.context.setLiftState(Context.stoppingState);  //设置为停止状态
        super.context.getLiftState().stop();
    }
}