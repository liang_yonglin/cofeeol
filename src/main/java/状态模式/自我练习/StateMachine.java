package 状态模式.自我练习;

import 状态模式.自我练习.状态.MyState;
import 状态模式.自我练习.状态.Pending;
import 状态模式.自我练习.状态.SendBack;

/**
 * 状态管理
 *
 * @author duguq
 */
public class StateMachine {
    public final static Pending PENDING = new Pending();
    public final static SendBack SEND_BACK = new SendBack();
    private MyState myState;

    public MyState getMyState() {
        return myState;
    }

    public void setMyState(MyState myState) {
        // 根据状态设置状态管理机器里面的MyState类型
        this.myState = myState;
        // 给新过渡的状态设置状态管理机器
        this.myState.setStateMachine(this);
    }

    public void pend() {
        this.getMyState().pend();
    }

    public void back() {
        this.getMyState().back();
    }
}
