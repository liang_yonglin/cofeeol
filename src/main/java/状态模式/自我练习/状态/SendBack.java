package 状态模式.自我练习.状态;

import 状态模式.自我练习.StateMachine;

/**
 * 审批退回状态
 *
 * @author duguq
 */
public class SendBack extends MyState {

    @Override
    public void back() {
        System.out.println("审批被驳回了");
    }

    @Override
    public void pend() {
        // 过渡到退回状态
        stateMachine.setMyState(StateMachine.PENDING);
        // 通过状态机获取到状态并执行操作
        MyState myState = stateMachine.getMyState();
        myState.pend();
    }
}
