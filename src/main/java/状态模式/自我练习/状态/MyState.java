package 状态模式.自我练习.状态;

import 状态模式.自我练习.StateMachine;

/**
 * @author duguq
 */
public abstract class MyState {

    /**
     * 每个状态都需要一个状态管理机器
     */
    protected StateMachine stateMachine;

    public void setStateMachine(StateMachine stateMachine) {
        this.stateMachine = stateMachine;
    }

    /**
     * 退回
     */
    public abstract void back();

    /**
     * 审批中
     */
    public abstract void pend();

}
