package 状态模式.自我练习.状态;

import 状态模式.自我练习.StateMachine;

/**
 * 审批中的状态
 *
 * @author duguq
 */
public class Pending extends MyState {

    @Override
    public void back() {
        // 过渡到退回状态
        stateMachine.setMyState(StateMachine.SEND_BACK);
        // 通过状态机获取到状态并执行操作
        MyState myState = stateMachine.getMyState();
        myState.back();
    }

    @Override
    public void pend() {
        System.out.println("审批在途中");
    }
}
