package 状态模式.自我练习;

import 状态模式.自我练习.状态.Pending;

/**
 * @author duguq
 */
public class Client {
    public static void main(String[] args) {
        StateMachine stateMachine = new StateMachine();
        stateMachine.setMyState(new Pending());
        stateMachine.back();
        stateMachine.pend();
    }

}
