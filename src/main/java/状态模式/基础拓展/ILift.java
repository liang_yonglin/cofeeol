package 状态模式.基础拓展;

public interface ILift {
    //电梯的4个状态
    /**
     * 敞门状态
     */
    int OPENING_STATE = 1;  //
    /**
     * 闭门状态
     */
    int CLOSING_STATE = 2;
    /**
     * 运行状态
     */
    int RUNNING_STATE = 3;
    /**
     * 停止状态
     */
    int STOPPING_STATE = 4;

    /**
     * 设置电梯的状态
     *
     * @param state
     */
    void setState(int state);

    /**
     * 首先电梯门开启动作
     */
    void open();

    /**
     * 电梯门关闭
     */
    void close();

    /**
     * 电梯要能上能下，运行起来
     */
    void run();

    /**
     * 电梯还要能停下来
     */
    void stop();
}