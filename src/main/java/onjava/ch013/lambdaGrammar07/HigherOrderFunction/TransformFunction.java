package onjava.ch013.lambdaGrammar07.HigherOrderFunction;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

class I {
    @Override
    public String toString() {
        return "I";
    }
}

class O {
    @Override
    public String toString() {
        return "O";
    }
}

public class TransformFunction {
    static Function<I, O> transform(Function<I, O> in) {
        return in.andThen(o -> {
            System.out.println(o);
            return o;
        });
    }


    public static void main(String[] args) {
        /*Function<I, O> f2 = transform(i -> {
            System.out.println(i);
            return new O();
        });
        O o = f2.apply(new I());*/
        Function<Integer, String> function = x -> {
            System.out.println("TransformFunction.apply");
            return x + "";
        };
        // function+andthen
        Function<Integer, List<String>> integerListFunction = function.andThen(s -> {
            ArrayList<String> strings = new ArrayList<>();
            strings.add(s);
            System.out.println("TransformFunction.andThen");
            return strings;
        });
        //  compose+(function+andthen)
        Function<String, List<String>> compose = integerListFunction.compose(y -> {
            System.out.println("TransformFunction.compose");
            return Integer.parseInt(y);
        });



//        String apply = compose.apply(10);
//        System.out.println("apply = " + apply);

        List<String> apply1 = compose.apply("10");

        System.out.println("apply1 = " + Function.identity().apply(4));
        List<UserAAA> userAAAS = Lists.newArrayList(new UserAAA("aa", "bb"), new UserAAA("cc", "dd"));
        Map<String, UserAAA> collect = userAAAS.stream().collect(Collectors.toMap(UserAAA::getName, Function.identity()));
        System.out.println("collect = " + collect);
    }

    @Data
    @AllArgsConstructor
    static class UserAAA{
        private String name;
        private String address;
    }
}