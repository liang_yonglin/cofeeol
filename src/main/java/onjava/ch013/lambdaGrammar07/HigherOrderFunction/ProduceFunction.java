package onjava.ch013.lambdaGrammar07.HigherOrderFunction;

import java.util.function.Function;

interface FuncSS extends Function<String, String> {
} // [1] 使用继承，可以轻松地为专用接口创建别名。

/**
 * 巧妙使用继承
 */
public class ProduceFunction {
    static FuncSS produce() {
        return s -> s.toLowerCase(); // [2] 使用 Lambda 表达式，可以轻松地在方法中创建和返回一个函数。要消费一个函数，消费函数需要在参数列表正确地描述函数类型。
    }

    public static void main(String[] args) {
        FuncSS f = produce();
        System.out.println(f.apply("YELLING"));
    }
}
