package onjava.ch013.lambdaGrammar03;

public interface IntCall {
    int call(int arg);
}