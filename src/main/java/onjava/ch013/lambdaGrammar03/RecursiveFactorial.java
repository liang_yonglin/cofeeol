package onjava.ch013.lambdaGrammar03;

public class RecursiveFactorial {
    /**
     * 为什么要用静态变量,因为这个是类共享的
     */
    static IntCall fact;

    public static void main(String[] args) {
        fact = n -> n == 0 ? 1 : n * fact.call(n - 1);
        for (int i = 0; i <= 10; i++)
            System.out.println(fact.call(i));
    }
}