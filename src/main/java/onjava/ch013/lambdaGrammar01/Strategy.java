package onjava.ch013.lambdaGrammar01;

/**
 * 一个策略
 */
public interface Strategy {
String approach(String msg);
}