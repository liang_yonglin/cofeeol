package onjava.ch013.lambdaGrammar01;

/**
 * 实现了策略
 */
public class Soft implements Strategy {
    /**
     * 某个方法
     *
     * @param msg
     * @return
     */
    public String approach(String msg) {
        return msg.toLowerCase() + "?";
    }
}
