package onjava.ch013.lambdaGrammar09.函数组合.柯里化;

import java.util.function.Function;

public class CurryingAndPartials {
    // 未柯里化:
    static String uncurried(String a, String b) {
        return a + b;
    }

    public static void main(String[] args) {
//        System.out.println(uncurried("Hi ", "Ho"));

        // 柯里化的函数:
        Function<String, Function<String, String>> sum =
                a -> b -> a + b; // [1] 这一连串的箭头很巧妙。注意，在函数接口声明中，第二个参数是另一个函数。
//        Function<String, String> hi =
//                sum.apply("Hi "); // [2] 柯里化的目的是能够通过提供单个参数来创建一个新函数，所以现在有了一个“带参函数” 和剩下的 “自由函数”（free argument）。实际上，你从一个双参数函数开始，最后得到一个单参数函数。
//        System.out.println(hi.apply("Ho"));

        // 部分应用:
        Function<String, String> sumHi =
                sum.apply("Hup ");
        System.out.println(sumHi.apply("Ho"));
        System.out.println(sumHi.apply("Hey"));
    }
}
