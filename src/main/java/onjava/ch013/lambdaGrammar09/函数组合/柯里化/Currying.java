package onjava.ch013.lambdaGrammar09.函数组合.柯里化;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.IntFunction;

public class Currying {

    // f(x,y,z)=x+y-z

    static Function<Integer, Function<Integer, Function<Integer, Integer>>> currying = new Function<Integer, Function<Integer, Function<Integer, Integer>>>() {
        @Override
        public Function<Integer, Function<Integer, Integer>> apply(Integer x) {
            return new Function<Integer, Function<Integer, Integer>>() {
                @Override
                public Function<Integer, Integer> apply(Integer y) {
                    return new Function<Integer, Integer>() {
                        @Override
                        public Integer apply(Integer z) {
                            return x + y - z;
                        }
                    };
                }
            };
        }
    };


    static Function<Integer, Function<Integer, Function<Integer, Integer>>> currying2 = x -> (Function<Integer, Function<Integer, Integer>>) y -> (Function<Integer, Integer>) z -> x + y - z;


    static Function<Integer, String> demo = integer -> String.valueOf(integer + integer);

    static Function<Integer, Function<String, List<Integer>>> demo2 = x -> (Function<String, List<Integer>>) y -> {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(Integer.parseInt(y));
        return arrayList;
    };

    // 使用IntFunction来验证自己的想法
    static IntFunction<IntFunction<IntFunction<Integer>>> demo3 = (IntFunction<IntFunction<IntFunction<Integer>>>) x -> (IntFunction<IntFunction<Integer>>) y -> (IntFunction<Integer>) z -> x + y + z;
    // 全部用推导,你就看不懂了,循序渐进能勉强看懂的
    static IntFunction<IntFunction<IntFunction<Integer>>> demo4 = x -> y -> z -> x + y + z;
    static IntFunction<IntFunction<IntFunction<Integer>>> demo5 = x -> (y -> (z -> x + y + z));


    public static void main(String[] arg) {
        //一次输入xyz
        System.out.println(currying.apply(10).apply(10).apply(5));
        //先输入x，返回f(y,z)
        Function<Integer, Function<Integer, Integer>> funcYandZ = currying.apply(10);
        //再输入y，返回f(z)
        Function<Integer, Integer> funcZ = funcYandZ.apply(10);
        //输入z
        System.out.println(funcZ.apply(5));
        //换一个z输入
        System.out.println(funcZ.apply(6));


        Integer apply = demo4.apply(100).apply(200).apply(50);
        System.out.println("apply = " + apply);
    }
}