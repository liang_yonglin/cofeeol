package onjava.ch013.lambdaGrammar04;

/**
 * 描述
 */
public class Describe {
    /**
     * 展示
     *
     * @param msg
     */
    void show(String msg) { // [2] show() 的签名（参数类型和返回类型）符合 Callable 的 call() 的签名。
        System.out.println("Describe" + msg);
    }
}