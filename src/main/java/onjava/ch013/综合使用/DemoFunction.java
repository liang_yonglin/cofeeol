package onjava.ch013.综合使用;

import java.util.function.BiFunction;
import java.util.function.Function;

public class DemoFunction {

    public static void main(String[] args) {
        DemoFunction t1 = new DemoFunction();
        // Function函数的使用
        /*Integer addResult = t1.compute(3, value -> value + value);
        System.out.println("加法结果：" + addResult); //3+3

        Integer subResult = t1.compute(3, value -> value - 1);
        System.out.println("减法结果：" + subResult); //3-1

        Integer multipResult = t1.compute(3, value -> value * value);
        System.out.println("乘法结果：" + multipResult); //3*3

        Integer divisionResult = t1.compute(6, value -> value / 3);
        System.out.println("除法结果：" + divisionResult); //6/3

        // 使用compose场景, 从右向左处理, 这里就是 (6 * 6) + 10 = 46
        Integer composeResult = t1.computeForCompose(6,
                value -> value + 10,
                value -> value * value);
        System.out.println("Function compose 结果：" + composeResult);

        // 使用andThen场景, 从左向右处理, 这里就是(3 + 20) - 10 = 13
        Integer andThenResult = t1.computeForAndThen(3,
                value -> value + 20,
                value -> value - 10);
        System.out.println("Function andThen 结果：" + andThenResult);*/


        // 使用 BiFunctioin场景, 这里是 2 + 3 = 5
        Integer biFuncResult = ((BiFunction<Integer, Integer, Integer>) new BiFunction<Integer, Integer, Integer>() {
            @Override
            public Integer apply(Integer v11, Integer v21) {
                return v11 + v21;
            }
        }).apply(2, 3);
        System.out.println("BiFunction 结果：" + biFuncResult);

        // 使用 BiFunctioin andThen场景, 这里是 (2 * 3) + 6 = 12
        Integer biFuncAndThenResult = ((BiFunction<Integer, Integer, Integer>) (v1, v2) -> v1 * v2).andThen(v11 -> v11 + 6).apply(2, 3);
        System.out.println("BiFunction andThen 结果：" + biFuncAndThenResult);

    }

    /**
     * 使用JDK8 Function函数
     *
     * @param num      入参
     * @param function 函数
     * @return Integer
     */
    private Integer compute(Integer num, Function<Integer, Integer> function) {
        return function.apply(num);
    }

    /**
     * 使用compose函数，简单的说，就是从右向左处理。
     *
     * @param num       变量
     * @param function1 函数1
     * @param function2 函数2
     * @return Integer
     */
    private Integer computeForCompose(Integer num,
                                      Function<Integer, Integer> function1,
                                      Function<Integer, Integer> function2) {
        return function1.compose(function2).apply(num);
    }

    /**
     * 使用andThen函数，简单的说，就是从左向右处理。
     *
     * @param num       变量
     * @param function1 函数1
     * @param function2 函数2
     * @return Integer
     */
    private Integer computeForAndThen(Integer num,
                                      Function<Integer, Integer> function1,
                                      Function<Integer, Integer> function2) {
        return function1.andThen(function2).apply(num);
    }

}