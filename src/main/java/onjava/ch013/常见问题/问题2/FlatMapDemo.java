package onjava.ch013.常见问题.问题2;

import cn.hutool.core.util.StrUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 具体需求是有个类，类里面有一个字段是使用逗号拼接的，类似一对多的关系
 * 把“多”拆分成一对一
 */
public class FlatMapDemo {
    public static void main(String[] args) {
        DemoDic demoDic1 = new DemoDic("01", "积分", "1,2,3");
        DemoDic demoDic2 = new DemoDic("02", "出行", "2,3");
        DemoDic demoDic3 = new DemoDic("03", "健康", "4");
        DemoDic demoDic4 = new DemoDic("04", "私人礼品", "5,6");
        List<DemoDic> dicArrayList = Lists.newArrayList(demoDic1, demoDic2, demoDic3, demoDic4);
        String[] strings = dicArrayList.stream().map(DemoDic::getCode).collect(Collectors.toList()).toArray(new String[0]);
        System.out.println("strings = " + Arrays.toString(strings));
        Map<String, List<DemoDic>> collect = dicArrayList.stream().collect(Collectors.groupingBy(DemoDic::getAttr1));
        Map<String, List<DemoDic>> eleMapList = Maps.newHashMap();
        collect.forEach((key, value) -> {
            List<String> split = StrUtil.split(key, ",");
            split.forEach(ele -> eleMapList.put(ele, value));
        });

        System.out.println("eleMapList = " + eleMapList);
    }
}
