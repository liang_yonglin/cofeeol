package onjava.ch013.常见问题.问题2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DemoDic {
    private String code;
    private String name;
    /**
     * 这里藏了多个
     */
    private String attr1;
}
