package onjava.ch013.常见问题.问题3map可重复key;

import com.google.common.collect.*;
import onjava.ch013.常见问题.问题2.DemoDic;

import java.util.List;
import java.util.function.Function;

/**
 * @author swqsv
 * @date 2022/9/1 15:18
 */
public class TestDemo {
    public static void main(String[] args) {
        DemoDic demoDic1 = new DemoDic("01", "积分", "1,2,3");
        DemoDic demoDic2 = new DemoDic("02", "出行", "2,3");
        DemoDic demoDic3 = new DemoDic("03", "健康", "4");
        DemoDic demoDic4 = new DemoDic("04", "私人礼品", "5,6");
        DemoDic demoDic5 = new DemoDic("05", "积分", "5,6");
        List<DemoDic> dicArrayList = Lists.newArrayList(demoDic1, demoDic2, demoDic3, demoDic4,demoDic5);



        HashMultimap<String, DemoDic> collect = dicArrayList.stream().
                collect(Multimaps.toMultimap(DemoDic::getName, Function.identity(),
                        HashMultimap::create));
        System.out.println("collect = " + collect);
    /*    Multimap<Integer, Integer> map = HashMultimap.create();
        map.put(1, 2);
        map.put(1, 3);
        map.put(1, 2);
        map.put(2, 3);
        map.put(4, 2);
        map.put(4, 3);
        map.put(4, 2);
        map.put(4, 3);
        Collection<Integer> integers = map.get(1);
        System.out.println(map.toString());*/
    }
}
