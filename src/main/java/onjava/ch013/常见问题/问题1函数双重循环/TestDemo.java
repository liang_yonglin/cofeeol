package onjava.ch013.常见问题.问题1函数双重循环;

import cn.hutool.core.util.StrUtil;
import com.google.common.collect.Lists;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.function.Consumer;

/**
 * @author swqsv
 * @date 2022/8/25 22:17
 */
public class TestDemo {
    public static void main(String[] args) {
        List<CostA> costAList = Lists.newArrayList(new CostA("123", "100.00"), new CostA("321", "200.00"));
        List<CostB> costBList = Lists.newArrayList(new CostB("321"), new CostB("123"));

/*        costBList.stream().<Consumer<? super CostA>>map(new Function<CostB, Consumer<? super CostA>>() {
            @Override
            public Consumer<? super CostA> apply(CostB costB) {
                System.out.println("111");
                return new Consumer<CostA>() {
                    @Override
                    public void accept(CostA costA) {
                        System.out.println("333");
                        String costACostId = costA.getCostId();
                        String costBCostId = costB.getCostId();
                        if (StrUtil.equals(costACostId, costBCostId)) {
                            BeanUtils.copyProperties(costA, costB);
                        }
                    }
                };
            }
        }).forEach(new Consumer<Consumer<? super CostA>>() {
            @Override
            public void accept(Consumer<? super CostA> action) {
                System.out.println("222");
                costAList.forEach(action);
            }
        });

        System.out.println("costBList = " + costBList);*/

//        costBList.stream().<Consumer<? super CostA>>map(costB -> costA -> {
//            String costACostId = costA.getCostId();
//            String costBCostId = costB.getCostId();
//            if (StrUtil.equals(costACostId, costBCostId)) {
//                BeanUtils.copyProperties(costA, costB);
//            }
//        }).forEach(costAList::forEach);
//
//        System.out.println("costBList = " + costBList);


        /*costBList.stream().map(costB -> (Consumer<CostA>) costA -> {
            String costACostId = costA.getCostId();
            String costBCostId = costB.getCostId();
            if (StrUtil.equals(costACostId, costBCostId)) {
                BeanUtils.copyProperties(costA, costB);
            }
        }).forEach(costAList::forEach);

        System.out.println("costBList = " + costBList);*/

      /*  costBList.stream().<Consumer<CostA>>map(costB -> costA -> {
            String costACostId = costA.getCostId();
            String costBCostId = costB.getCostId();
            if (StrUtil.equals(costACostId, costBCostId)) {
                BeanUtils.copyProperties(costA, costB);
            }
        }).forEach(costAList::forEach);

        System.out.println("costBList = " + costBList);*/

        costBList.stream().map(b -> (Consumer<CostA>) a -> {
            String costACostId = a.getCostId();
            String costBCostId = b.getCostId();
            if (StrUtil.equals(costACostId, costBCostId)) {
                BeanUtils.copyProperties(a, b);
            }
        }).forEach(costAList::forEach);

    }


}
