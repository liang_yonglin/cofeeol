package onjava.ch013.常见问题.问题1函数双重循环;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author swqsv
 * @date 2022/8/25 22:17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CostA {
    private String costId;
    private String amount;
}
