package onjava.ch013.lambdaGrammar02;

public interface Multi {
    /**
     * 二者兼备
     *
     * @param head
     * @param d
     * @return
     */
    String twoArg(String head, Double d);
}