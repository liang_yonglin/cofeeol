package onjava.ch013.lambdaGrammar02;

public interface Body {
    /**
     * 详细的
     * @param head
     * @return
     */
    String detailed(String head);
}