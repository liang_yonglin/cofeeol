package onjava.ch013.lambdaGrammar02;

public interface Description {
    /**
     * 简短的
     * @return
     */
    String brief();
}