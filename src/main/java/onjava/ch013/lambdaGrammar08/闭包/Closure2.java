package onjava.ch013.lambdaGrammar08.闭包;

import java.util.function.IntSupplier;

public class Closure2 {
    /**
     * 这里的x没有改变，所以是等效于final的
     * 等同 final 效果（Effectively Final）。这个术语是在 Java 8 才开始出现的，表示虽然没有明确地声明变量是 final 的，但是因变量值没被改变过而实际有了 final同等的效果。
     * @param x
     * @return
     */
    IntSupplier makeFun(int x) {
        int i = 0;
        return () -> x + i;
    }
}
