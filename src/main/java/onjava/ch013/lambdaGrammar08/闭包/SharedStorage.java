package onjava.ch013.lambdaGrammar08.闭包;

import java.util.List;
import java.util.function.IntSupplier;

/**
 * 实际上只要有内部类，就会有闭包
 */
public class SharedStorage {
    public static void main(String[] args) {
//        Closure1 c1 = new Closure1();
//        IntSupplier f1 = c1.makeFun(0);
//        IntSupplier f2 = c1.makeFun(0);
//        IntSupplier f3 = c1.makeFun(0);
//        System.out.println(f1.getAsInt());
//        System.out.println(f2.getAsInt());
//        System.out.println(f3.getAsInt());

        /*闭包2*/
//        Closure2 closure2 = new Closure2();
//        IntSupplier intSupplier = closure2.makeFun(1);

        /*闭包6*/
//        Closure6 closure6 = new Closure6();
//        IntSupplier intSupplier = closure6.makeFun(1);
//        System.out.println("intSupplier.getAsInt() = " + intSupplier.getAsInt());

        Closure8 closure8 = new Closure8();
        List<Integer>
                l1 = closure8.makeFun().get(),
                l2 = closure8.makeFun().get();
        System.out.println(l1);
        System.out.println(l2);
        l1.add(42);
        l2.add(96);
        System.out.println(l1);
        System.out.println(l2);

    }
}
