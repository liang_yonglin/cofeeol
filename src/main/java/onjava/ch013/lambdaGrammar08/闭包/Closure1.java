package onjava.ch013.lambdaGrammar08.闭包;

import java.util.function.IntSupplier;

/**
 * Closure是闭包的意思
 */
public class Closure1 {
    int i;

    IntSupplier makeFun(int x) {
        return () -> x + i++;
    }
}
