package onjava.ch013.lambdaGrammar08.闭包举例;

public class Milk {

    public final static String name = "纯牛奶";//名称

    private static int num = 16;//数量

    public Milk() {
        System.out.println(name + "：16/每箱");
    }

    /**
     * 闭包
     *
     * @return 返回一个喝牛奶的动作
     */
    public Active HaveMeals() {
        return new Active() {
            public void drink() {
                if (num == 0) {
                    System.out.println("木有了，都被你丫喝完了.");
                    return;
                }
                num--;
                System.out.println("喝掉一瓶牛奶");
            }
        };
    }

    /**
     * 获取剩余数量
     */
    public void currentNum() {
        System.out.println(name + "剩余：" + num);
    }
}


//运行结果
//
//
//  纯牛奶：16/每箱
//  喝掉一瓶牛奶
//  喝掉一瓶牛奶
//  纯牛奶剩余：14
//
//
//  上述例子中，通过调用Active的方法实现对Milk私有变量num进行修改。
//
//  有时候觉得直接使用set方法也可以直接修改private变量，但是从现实生活中来说让人去执行喝牛奶的动作比牛奶自己动手喝来的合理一些。
//
//  总结
//
//  1.实际项目中没怎么用过闭包，因此不能对他的好坏进行评论。
//
//  2.建议合理的使用闭包，不完全不使用，也不能滥用。
//
//  3.特别注意：闭包会导致资源不被回收，如上例，在main方法中将m设为null，使用haveMeals继续调用drink方法仍然会喝掉一瓶牛奶，说明Milk对象并没有被释放掉。