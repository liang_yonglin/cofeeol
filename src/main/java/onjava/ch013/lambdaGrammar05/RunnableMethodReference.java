package onjava.ch013.lambdaGrammar05;

public class RunnableMethodReference {
    public static void main(String[] args) {
        new Thread(new Runnable() {
            public void run() {
                System.out.println("Anonymous");
            }
        }).start();

        new Thread(
                () -> System.out.println("lambda")
        ).start();

        // 很明显这里将GO的go方法映射到Runnable的run上,如果你不知道这一点,你就但不懂这段代码
        new Thread(Go::go).start();
    }
}