package onjava.ch013.lambdaGrammar06.demo03构造函数引用;

class Dog {
    String name;
    int age = -1; // For "unknown"

    Dog() {
        name = "stray";
        System.out.println("我是无参构造函数哦");
    }

    Dog(String nm) {
        name = nm;
        System.out.println("nm = " + nm);
    }

    Dog(String nm, int yrs) {
        name = nm;
        age = yrs;
        System.out.println("nm = " + nm + ", yrs = " + yrs);
    }
}





