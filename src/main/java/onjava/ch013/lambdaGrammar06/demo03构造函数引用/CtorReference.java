package onjava.ch013.lambdaGrammar06.demo03构造函数引用;

/**
 * 捕获构造函数的引用，然后通过引用调用该构造函数。
 *
 * @author swqsv
 * @date 2022/8/7 16:37
 */
public class CtorReference {
    public static void main(String[] args) {
        MakeNoArgs mna = Dog::new; // [1]
        Make1Arg m1a = Dog::new; // [2]
        Make2Args m2a = Dog::new; // [3]
        Dog dn = mna.make();
        Dog d1 = m1a.make("Comet");
        Dog d2 = m2a.make("Ralph", 4);
    }
}

