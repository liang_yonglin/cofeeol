package onjava.ch013.lambdaGrammar06;

import java.util.function.BiConsumer;

class In1 {
}

class In2 {
}

public class MethodConversion {
    static void accept(In1 i1, In2 i2) {
        System.out.println("accept()");
    }

    static void someOtherName(In1 i1, In2 i2) {
        System.out.println("someOtherName()");
    }

    /**
     * 只要参数和返回能对上就行,方法签名的名称不重要
     *
     * @param args
     */
    public static void main(String[] args) {
        BiConsumer<In1, In2> bic;
        bic = MethodConversion::accept;
        bic.accept(new In1(), new In2());


        bic = MethodConversion::someOtherName;
// bic.someOtherName(new In1(), new In2()); // Nope
        bic.accept(new In1(), new In2());
    }
}