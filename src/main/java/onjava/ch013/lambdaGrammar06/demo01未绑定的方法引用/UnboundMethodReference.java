package onjava.ch013.lambdaGrammar06.demo01未绑定的方法引用;

/**
 * 未绑定的方法引用是指没有关联对象的普通（非静态）方法。
 * 1.java当中的方法只有final，static，private和构造方法是前期绑定
 * 2.其他类型在运行时根据具体对象的类型进行绑定
 */
public class UnboundMethodReference {
    public static void main(String[] args) {
//         MakeString ms = X::f; // [1] 无法从 static 上下文引用非 static 方法

        /*---分割线---*/
        // X中的f()应该是非静态的,但是却能类似静态被使用,原因是依托在了TransformX上,X通过参数的形式传递,世界上X仍然需要new出来,然后f()映射到transform()上面,transform(X x)的x起到初始化的作用,用来应用没有绑定的方法;
        TransformX sp = X::f;
        X x = new X();
        System.out.println(sp.transform(x)); // [2] 的结果有点像脑筋急转弯。我拿到未绑定的方法引用，并且调用它的transform() 方法，将一个 X 类的对象传递给它，最后使得 x.f() 以某种方式被调用。Java 知道它必须拿到第一个参数，该参数实际就是 this，然后调用方法作用在它之上。
        System.out.println(x.f()); // 同等效果
    }
}
