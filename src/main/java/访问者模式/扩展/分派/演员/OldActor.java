package 访问者模式.扩展.分派.演员;

import 访问者模式.扩展.分派.角色.KungFuRole;

/**
 * @author duguq
 */
public class OldActor extends AbsActor {
    /**
     * 不演功夫角色
     *
     * @param role
     */
    @Override
    public void act(KungFuRole role) {
        System.out.println("年龄大了，不能演功夫角色");
    }
}