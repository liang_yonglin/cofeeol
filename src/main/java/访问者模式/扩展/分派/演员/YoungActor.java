package 访问者模式.扩展.分派.演员;

import 访问者模式.扩展.分派.角色.KungFuRole;

/**
 * @author duguq
 */
public class YoungActor extends AbsActor {
    /**
     * 年轻演员最喜欢演功夫戏
     *
     * @param role
     */
    @Override
    public void act(KungFuRole role) {
        System.out.println("最喜欢演功夫角色");
    }
}