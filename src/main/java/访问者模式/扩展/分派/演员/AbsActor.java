package 访问者模式.扩展.分派.演员;

import 访问者模式.扩展.分派.角色.KungFuRole;
import 访问者模式.扩展.分派.角色.Role;

/**
 * @author duguq
 */
public abstract class AbsActor {
    /**
     * 演员都能够演一个角色
     *
     * @param role
     */
    public void act(Role role) { // 如果是role进行多态表示就走此方法  方法名和参数列表组合起来-->方法签名
        System.out.println("演员可以扮演任何角色");
    }

    /**
     * 可以演功夫戏
     *
     * @param role
     */
    public void act(KungFuRole role) { // 如果new kungfurole就走此方法
        System.out.println("演员都可以演功夫角色");
    }
}