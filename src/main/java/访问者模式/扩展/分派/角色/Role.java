package 访问者模式.扩展.分派.角色;

import 访问者模式.扩展.分派.演员.AbsActor;

/**
 * 演员要扮演的角色
 *
 * @author duguq
 */
public interface Role {
    /**
     * 使用访问者模式
     *
     * @param actor
     */
    void accept(AbsActor actor);
}