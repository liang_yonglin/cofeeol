package 访问者模式.扩展.分派.角色;

import 访问者模式.扩展.分派.演员.AbsActor;

/**
 * @author duguq
 * 一个弱智角色
 */
public class IdiotRole implements Role {
    /**
     * 使用访问者模式
     *
     * @param actor
     */
    @Override
    public void accept(AbsActor actor) {
        actor.act(this);
    }
}