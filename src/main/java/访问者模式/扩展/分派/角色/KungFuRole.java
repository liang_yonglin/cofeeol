package 访问者模式.扩展.分派.角色;

import 访问者模式.扩展.分派.演员.AbsActor;

/**
 * 武功天下第一的角色
 *
 * @author duguq
 */
public class KungFuRole implements Role {
    /**
     * 使用访问者模式
     *
     * @param actor
     */
    @Override
    public void accept(AbsActor actor) {
        actor.act(this);
    }
}