package 访问者模式.扩展.分派;

import 访问者模式.扩展.分派.演员.AbsActor;
import 访问者模式.扩展.分派.演员.OldActor;
import 访问者模式.扩展.分派.角色.KungFuRole;
import 访问者模式.扩展.分派.角色.Role;

/**
 * @author duguq
 */
public class Client {
    public static void main(String[] args) {
        //一般模式
        //定义一个演员
        // AbsActor oldActor = new OldActor();
        // AbsActor youngActor = new YoungActor();
        // 1. 两个角色都是用new
        // oldActor.act(new KungFuRole()); //年龄大了，不能演功夫角色-->Actor的执行方法act则是由其实际类型决定的，这是动态绑定。
        // youngActor.act(new KungFuRole());//最喜欢演功夫角色
        // 2. 两个角色都使用kungfu;   1 和 2 是等价的
        // KungFuRole kungFuRole = new KungFuRole();
        // oldActor.act(kungFuRole);//年龄大了，不能演功夫角色
        // youngActor.act(kungFuRole);//最喜欢演功夫角色

        // 3.两种角色都使用role来进行多态表示
        // Role role = new KungFuRole();
        // oldActor.act(role);// 演员可以扮演任何角色-->重载在编译器期就决定了要调用哪个方法，它是根据role的表面类型而决定调用act(Role role)方法，这是静态绑定；
        // youngActor.act(role);//演员可以扮演任何角色

        //访问者模式;
        // 不管演员类和角色类怎么变化，我们都能够找到期望的方法运行，这就是双反派。
        // 双分派意味着得到执行的操作决定于请求的种类和两个接收者的类型，它是多分派的一个特例。
        AbsActor actor = new OldActor();
        Role role = new KungFuRole();
        role.accept(actor);//年龄大了，不能演功夫角色

    }
}