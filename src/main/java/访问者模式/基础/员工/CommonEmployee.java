package 访问者模式.基础.员工;

import 访问者模式.基础.访问者.IVisitor;

/**
 * 普通员工
 *
 * @author duguq
 */
public class CommonEmployee extends Employee {
    /**
     * 工作内容，这非常重要，以后的职业规划就是靠它了
     */
    private String job;

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    /**
     * 允许访问者访问
     *
     * @param visitor
     */
    @Override
    public void accept(IVisitor visitor) {
        visitor.visit(this);
    }
}