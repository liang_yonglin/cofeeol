package 访问者模式.基础.员工;

import 访问者模式.基础.访问者.IVisitor;

/**
 * 经理
 *
 * @author duguq
 */
public class Manager extends Employee {
    /**
     * 业绩
     */
    private String performance;

    public String getPerformance() {
        return performance;
    }

    public void setPerformance(String performance) {
        this.performance = performance;
    }

    /**
     * 部门经理允许访问者访问
     *
     * @param visitor
     */
    @Override
    public void accept(IVisitor visitor) {
        visitor.visit(this);
    }
}