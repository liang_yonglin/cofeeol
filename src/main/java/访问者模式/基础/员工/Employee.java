package 访问者模式.基础.员工;

import 访问者模式.基础.访问者.IVisitor;

/**
 * @author duguq
 */
public abstract class Employee {
    /**
     * 0代表是男性
     */
    public final static int MALE = 0;
    /**
     * 1代表是女性
     */
    public final static int FEMALE = 1;
    /**
     * 名字
     */
    private String name;
    /**
     * 薪水
     */
    private int salary;
    /**
     * 性别
     */
    private int sex;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    /**
     * 允许一个访问者访问
     *
     * @param visitor
     */
    public abstract void accept(IVisitor visitor);
}