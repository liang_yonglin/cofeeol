package 访问者模式.基础.访问者;

import 访问者模式.基础.员工.CommonEmployee;
import 访问者模式.基础.员工.Manager;

/**
 * 该接口的意义是：该接口可以访问两个对象，一个是普通员工，一个是高层员工。
 *
 * @author duguq
 */
public interface IVisitor {
    /**
     * 可以访问普通员工
     *
     * @param commonEmployee
     */
    void visit(CommonEmployee commonEmployee);

    /**
     * 访问部门经理
     *
     * @param manager
     */
    void visit(Manager manager);
}