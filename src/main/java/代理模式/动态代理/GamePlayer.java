package 代理模式.动态代理;


import java.util.StringJoiner;

public class GamePlayer implements IGamePlayer {
    private final String name;

    public GamePlayer(String _name) {
        this.name = _name;
    }

    //打怪，最期望的就是杀老怪
    public void killBoss() {
        System.out.println("请使用指定的代理访问");
    }

    //进游戏之前你肯定要登录吧，这是一个必要条件
    public void login(String user, String password) {
        System.out.println("请使用指定的代理访问");
    }

    //升级，升级有很多方法，花钱买是一种，做任务也是一种
    public void upgrade() {
        System.out.println("请使用指定的代理访问");
    }


    @Override
    public String toString() {
        return new StringJoiner(", ", GamePlayer.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .toString();
    }

}