package 代理模式.动态代理.自我练习cglib;


/**
 * 买鞋人
 */
public class ShoesShopper {
    private String name;

    public ShoesShopper(String name) {
        this.name = name;
    }

    public ShoesShopper() {
    }

    public void watch() {
        System.out.println(name + "看看了鞋子");
    }

    public void compared() {
        System.out.println(name + "对比了一下鞋子");
    }

    public void buy() {
        System.out.println(name + "终于买了鞋了");
    }
}
