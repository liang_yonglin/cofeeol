package 代理模式.动态代理.自我练习cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * 代购  另辟蹊径,独成一派
 */
public class ProxyShopper implements MethodInterceptor {

    public Object getProxy(Class cls) {
        // CGLIB <u>enhancer</u>增强类对象
        Enhancer enhancer = new Enhancer();
        // 设置增强类型
        enhancer.setSuperclass(cls);
        // 定义代理逻辑对象为当前对象，要求当前对象实现MethodInterceptor方法
        enhancer.setCallback(this);
        // 生成并返回代理对象

        return enhancer.create();
    }

    @Override
    public Object intercept(Object o, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        if (true) {
            //执行一个前置通知
            (new BeforeAdvice()).exec();
        }
        Object o1 = methodProxy.invokeSuper(o, args);
        if (true) {
            //执行一个前置通知
            (new AfterAdvice()).exec();
        }
        return o1;

    }
}
