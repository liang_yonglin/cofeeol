package 代理模式.动态代理.自我练习cglib;


import net.sf.cglib.proxy.Enhancer;

/**
 * 调用方
 */
public class CallClient {
    public static void main(String[] args) {
      /*  ShoesShopper yx = (ShoesShopper) Enhancer.create(ShoesShopper.class,
                new ProxyShopper(new ShoesShopper("孙文")));
        yx.watch();
        yx.compared();
        yx.buy();*/

        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(ShoesShopper.class);
        //设置回调函数
        enhancer.setCallback(new ProxyShopper());

        //这里的creat方法就是正式创建代理类
        ShoesShopper yx = (ShoesShopper) enhancer.create(new Class[]{String.class}, new Object[]{"孙文"});
        //调用代理类的eat方法
        yx.watch();
        yx.compared();
        yx.buy();
    }
}
