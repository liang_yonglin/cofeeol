package 代理模式.动态代理.自我练习cglib;

public interface IAdvice {
    //通知只有一个方法，执行即可
    void exec();
}