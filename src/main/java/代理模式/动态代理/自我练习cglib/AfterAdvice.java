package 代理模式.动态代理.自我练习cglib;

public class AfterAdvice implements IAdvice {
    public void exec() {
        System.out.println("我是后置通知，我被执行了！");
    }
}