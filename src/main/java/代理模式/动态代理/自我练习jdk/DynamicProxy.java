package 代理模式.动态代理.自我练习jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class DynamicProxy {
    public static <T> T newProxyInstance(ClassLoader loader, Class<?>[] interfaces, InvocationHandler h) {
        T t = (T) Proxy.newProxyInstance(loader, interfaces, h);
        return t;
    }
}