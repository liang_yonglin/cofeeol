package 代理模式.动态代理.自我练习jdk;


/**
 * 买鞋人
 */
public class ShoesShopper implements IShoper {
    private final String name;

    public ShoesShopper(String name) {
        this.name = name;
    }


    @Override
    public void watch() {
        System.out.println(name + "看看了鞋子");
    }

    @Override
    public void compared() {
        System.out.println(name + "对比了一下鞋子");
    }

    @Override
    public void buy() {
        System.out.println(name + "终于买了鞋了");
    }
}
