package 代理模式.动态代理.自我练习jdk;

/**
 * 调用方
 */
public class CallClient {
    public static void main(String[] args) {
        /*IShoper yx = (IShoper) Proxy.newProxyInstance(ShoesShopper.class.getClassLoader(),
                ShoesShopper.class.getInterfaces(),
                new ProxyShopper(new ShoesShopper("孙文")));
        yx.watch();
        yx.compared();
        yx.buy();*/
        IShoper yx = DynamicProxy.newProxyInstance(ShoesShopper.class.getClassLoader(),
                ShoesShopper.class.getInterfaces(),
                new ProxyShopper(new ShoesShopper("孙文")));
        yx.watch();
        yx.compared();
        yx.buy();
    }
}
