package 代理模式.动态代理.自我练习jdk;

import 代理模式.动态代理.自我练习cglib.AfterAdvice;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 代购  另辟蹊径,独成一派
 */
public class ProxyShopper implements InvocationHandler {
    private Object obj;

    public ProxyShopper(Object object) {
        this.obj = object;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println(proxy.getClass().getName());
        //这里负责调用
        //寻找JoinPoint连接点，AOP框架使用元数据定义
        if (true) {
            //执行一个前置通知
            (new BeforeAdvice()).exec();
        }
        Object invoke = method.invoke(obj, args);
        if (true) {
            //执行一个前置通知
            (new AfterAdvice()).exec();
        }
        return invoke;
    }
}
