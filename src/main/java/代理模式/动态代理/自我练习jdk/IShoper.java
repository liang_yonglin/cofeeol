package 代理模式.动态代理.自我练习jdk;

/**
 * 一个购物者的接口,当然下面会有很多人会实现他,才要定义接口,不然只有自己玩不就是过度设计了吗
 */
public interface IShoper {
    /**
     * 看
     */
    void watch();

    /**
     * 对比
     */
    void compared();

    /**
     * 购买
     */
    void buy();

}
