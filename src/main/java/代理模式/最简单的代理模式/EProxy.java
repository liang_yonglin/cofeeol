package 代理模式.最简单的代理模式;

/**
 * E代理类
 */
public class EProxy extends EsgController {

    @Override
    public void MethodA() {
        this.before();
        super.MethodA();
        this.after();
    }

    //预处理
    private void before() {
        System.out.println("执头");
    }

    //善后处理
    private void after() {
        System.out.println("执尾");
    }
}
