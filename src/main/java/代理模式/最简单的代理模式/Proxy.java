package 代理模式.最简单的代理模式;

/**
 * 代理类
 */
public class Proxy implements Icontroller {
    //被代理类,被代理类肯定也实现这Icontroller
    private Icontroller icontroller = null;

    public Proxy(Icontroller icontroller) {
        this.icontroller = icontroller;//初始化的时候就把自己变成代理
    }

    @Override
    public void MethodA() {
        this.before();
        this.icontroller.MethodA();
        this.after();
    }

    @Override
    public void MethodB() {

    }

    @Override
    public void MethodC() {

    }

    //预处理
    private void before() {
        System.out.println("执头");
    }

    //善后处理
    private void after() {
        System.out.println("执尾");
    }
}
