package 代理模式.最简单的代理模式;

/**
 * 这种代理需要知道被代理的类和代理类
 */
public class Client {
    public static void main(String[] args) {
        /*//定义一个痴迷的玩家
        Icontroller icontroller = new EsgController();
        //然后再定义一个代练者
        Icontroller proxy = new Proxy(icontroller);
        //开始打游戏，记下时间戳
        proxy.MethodA();*/
        EProxy eProxy = new EProxy();
        eProxy.MethodA();
    }
}