package 代理模式.普通代理;

public class GamePlayerProxy implements IGamePlayer {
    private IGamePlayer gamePlayer = null;

    //通过构造函数传递要对谁进行代练
    public GamePlayerProxy(String name) {
        try {
            gamePlayer = new GamePlayer(this, name);//这里就持有了真实类
        } catch (Exception e) {
            // TODO 异常处理
        }
    }

    //代练杀怪
    public void killBoss() {
        gamePlayer.killBoss();
    }

    //代练登录
    public void login(String user, String password) {
        gamePlayer.login(user, password);
    }

    //代练升级
    public void upgrade() {
        gamePlayer.upgrade();
    }
}