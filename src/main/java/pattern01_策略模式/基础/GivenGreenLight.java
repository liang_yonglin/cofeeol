package pattern01_策略模式.基础;

public class GivenGreenLight implements IStrategy {
    public void operate() {
        System.out.println("求吴国太开绿灯,放行！");
    }
}