package pattern01_策略模式.HeadFirst策略模式学习;

/**
 * 橡皮鸭子才会吱吱叫
 * @author swqsv
 * @date 2022/8/31 23:06
 */
public class Squeak implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("吱吱叫");
    }
}
