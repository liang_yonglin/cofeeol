package pattern01_策略模式.HeadFirst策略模式学习;

/**
 * 飞行的相关行为
 *
 * @author swqsv
 * @date 2022/8/31 23:02
 */
public interface FlyBehavior {
    /**
     * 描述如何飞行的
     */
    void fly();
}
