package pattern01_策略模式.HeadFirst策略模式学习;

/**
 * @author swqsv
 * @date 2022/9/1 20:52
 */
public class FlyRocketPowered implements FlyBehavior {

    @Override
    public void fly() {
        System.out.println("加上了火箭助推器");
    }
}
