package pattern01_策略模式.HeadFirst策略模式学习;

/**
 * @author swqsv
 * @date 2022/8/31 23:03
 */
public class FlyWithWings implements FlyBehavior {

    @Override
    public void fly() {
        System.out.println("鸭子飞起来了");
    }
}
