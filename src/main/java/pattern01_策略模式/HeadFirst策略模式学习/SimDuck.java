package pattern01_策略模式.HeadFirst策略模式学习;

/**
 * @author swqsv
 * @date 2022/9/1 0:18
 */
public class SimDuck {
    public static void main(String[] args) {
        Duck mallardDuck = new MallardDuck();
        mallardDuck.performFly();
        mallardDuck.performQuack();

        // 加上getter以后就能动态设置属性了
        Duck modelDuck = new ModelDuck();
        modelDuck.performFly();
        modelDuck.performQuack();
        modelDuck.setFlyBehavior(new FlyRocketPowered());
        modelDuck.performFly();
    }
}
