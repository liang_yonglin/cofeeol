package pattern01_策略模式.HeadFirst策略模式学习;

/**
 * @author swqsv
 * @date 2022/9/1 20:50
 */
public class ModelDuck extends Duck {
    public ModelDuck() {
        flyBehavior = new FlyNoWay();
        quackBehavior = new MuteQuack();
    }

    @Override
    void display() {
        System.out.println("这是一只模型鸭子");
    }
}
