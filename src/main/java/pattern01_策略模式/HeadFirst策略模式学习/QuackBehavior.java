package pattern01_策略模式.HeadFirst策略模式学习;

/**
 * 嘎嘎叫的相关行为
 *
 * @author swqsv
 * @date 2022/8/31 23:02
 */
public interface QuackBehavior {
    /**
     * 描述如何叫的
     */
    void quack();
}
