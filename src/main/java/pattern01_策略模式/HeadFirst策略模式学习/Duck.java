package pattern01_策略模式.HeadFirst策略模式学习;

/**
 * 面对接口编程,实际上接口只是一个概念,准确来说是面对超类编程
 * <p>
 * 将变化与不变区分开
 * 变化的委托到别的引用对象去做
 * 不变的可以写在自己的类里面
 *
 * @author swqsv
 * @date 2022/8/31 23:12
 */
public abstract class Duck {
    public Duck() {
    }

    /**
     * 飞行是变化的,所以委托给别的类
     */
    FlyBehavior flyBehavior;
    /**
     * 嘎嘎叫是变化的,所以委托给别的类
     */
    QuackBehavior quackBehavior;

    void swim() {
        System.out.println("鸭子在游泳");
    }

    void display() {
        System.out.println("鸭子在玩");
    }

    /**
     * 表演嘎嘎叫
     */
    void performQuack() {
        quackBehavior.quack();
    }

    /**
     * 表演飞行
     */
    void performFly() {
        flyBehavior.fly();
    }

    public void setFlyBehavior(FlyBehavior flyBehavior) {
        this.flyBehavior = flyBehavior;
    }

    public void setQuackBehavior(QuackBehavior quackBehavior) {
        this.quackBehavior = quackBehavior;
    }
}
