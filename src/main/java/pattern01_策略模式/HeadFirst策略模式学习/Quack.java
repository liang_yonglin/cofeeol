package pattern01_策略模式.HeadFirst策略模式学习;

/**
 * 真的鸭子会嘎嘎叫
 *
 * @author swqsv
 * @date 2022/8/31 23:04
 */
public class Quack implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("鸭子嘎嘎叫");
    }
}
