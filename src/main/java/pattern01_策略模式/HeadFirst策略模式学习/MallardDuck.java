package pattern01_策略模式.HeadFirst策略模式学习;

/**
 * @author swqsv
 * @date 2022/8/31 23:40
 */
public class MallardDuck extends Duck {
    public MallardDuck() {
        quackBehavior = new Quack();
        flyBehavior = new FlyWithWings();
    }


    @Override
    void display() {
        System.out.println("很明显是个绿头鸭");
    }
}
